# 制造执行系统

#### 介绍
基于spring cloud 制造执行系统(MES)

#### 演示环境(暂停)
地址： [http://mes-client.web.bpm.plus/](http://mes-client.web.bpm.plus/)  
用户名密码: admin/123456

#### 公司商业版
PC端  
http://show.tltim.com.cn:9993  
用户名密码：admin/donger@2020  
PAD端  
http://show.tltim.com.cn:9988  
用户名/密码：19384/123456a?  


#### 软件架构
| 依赖                   | 版本       |
|----------------------|----------|
| Spring Boot          | 2.5.6    |
| Spring Cloud         | 2020.0.3 |
| Spring Cloud Alibaba | 2021.1   |
| sa token             | 1.27.0   |
| Mybatis Plus         | 3.4.2    |
| hutool               | 5.7.0    |
| ant design vue       | 1.5.0    |


#### 依赖
 - common 工具类[https://gitee.com/financial-media-development/common]
 - sso-server 单点登录系统[https://gitee.com/financial-media-development/sso-server]




#### 功能说明

1. 计划管理
    - 日工单计划
      - 工单执行详情
    - 工作中心计划
      - 报工台
2. 生产管理
    - 生产物料信息
    - 生产产品信息
3. 生产过程管理
    - 投料记录
    - 报工记录
    - 领料单
4. 线边管理
    - 库位管理
    - 实时库存管理
    - 出入库日志
    - 出入库订单
6. 基础信息管理
    - 物料管理
    - bom管理
    - 产线管理
    - 工作中心
    - 设备管理
    - 工位管理

### 功能截图
1. 计划可视化
   ![](./doc/images/gantt.png)
2. 每日工单
   ![](./doc/images/planDay.png)
3. 工单生产详情
   ![](./doc/images/工单详情.png)
4. 齐套检查
   ![](./doc/images/齐套检查.png)
5. 投料记录
   ![](./doc/images/投料记录.png)
6. 报工记录
   ![](./doc/images/报工记录.png)
7. 线边库实时记录
   ![](./doc/images/线边实时物料.png)
8. 出入库记录
   ![](./doc/images/出入库记录.png)
9. BOM信息
   ![](./doc/images/bom管理.png)

