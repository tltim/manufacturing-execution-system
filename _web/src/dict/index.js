export const DataScopeTypeList = [
  {
    label: '全部',
    value: '0'
  },
  {
    label: '自定义',
    value: '1'
  },
  {
    label: '本级',
    value: '3'
  },
  {
    label: '本级及子级',
    value: '2'
  }
]

export const statusTypeList = [
  {
    label: '禁用',
    value: '0'
  },
  {
    label: '正常',
    value: '1'
  }
]

export const PayChannelNameList = [
  {
    label: '支付宝手机支付',
    value: 'ALIPAY_WAP'
  },
  {
    label: '微信H5支付',
    value: 'WEIXIN_WAP'
  },
  {
    label: '微信公众号支付',
    value: 'WEIXIN_MP'
  }
]

export const orderStatusList = [
  {
    label: '下单',
    value: '0'
  },
  {
    label: '支付成功',
    value: '1'
  },
  {
    label: '支付完成',
    value: '2'
  }, {
    label: '支付失败',
    value: '-1'
  }, {
    label: '订单取消',
    value: '-2'
  }
]

export const authorizedGrantTypeList = [
  {
    label: 'refresh_token',
    value: 'refresh_token'
  },
  {
    label: 'password',
    value: 'password'
  },
  {
    label: 'authorization_code',
    value: 'authorization_code'
  },
  {
    label: 'mobile',
    value: 'mobile'
  },
  {
    label: 'implicit',
    value: 'implicit'
  },
  {
    label: 'client_credentials',
    value: 'client_credentials'
  }
]
