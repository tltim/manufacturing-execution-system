import BaseSelect from './BaseSelect'
import BaseInput from './BaseInput'
import BaseDate from './BaseDate'
import BaseSelectTree from './BaseSelectTree'
import BaseRadioGroup from './BaseRadioGroup'

export default [
  BaseSelect,
  BaseInput,
  BaseDate,
  BaseSelectTree,
  BaseRadioGroup
]
