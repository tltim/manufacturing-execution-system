import VTable from './VxeTableDemo'
VTable.install = function (Vue) {
  Vue.component(VTable.name, VTable)
}
export default VTable