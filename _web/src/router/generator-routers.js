// eslint-disable-next-line
import { menuNav } from '@/api/login'
// eslint-disable-next-line
import { BasicLayout, BlankLayout, PageView, RouteView } from '@/layouts'

// 前端路由表
const constantRouterComponents = {
  // 基础页面 layout 必须引入
  "BasicLayout": () => import('@/layouts/BasicLayout'),
  "BlankLayout": () => import('@/layouts/BlankLayout'),
  "RouteView": () => import('@/layouts/RouteView'),
  "PageView": () => import('@/layouts/PageView'),
  '403': () => import(/* webpackChunkName: "error" */ '@/views/exception/403'),
  '404': () => import(/* webpackChunkName: "error" */ '@/views/exception/404'),
  '500': () => import(/* webpackChunkName: "error" */ '@/views/exception/500'),

  'Analysis': () => import('@/views/dashboard/Analysis'),

  'ChangeLog': () => import('@/views/dashboard/ChangeLog'),
  'AccountSettings': () => import('@/views/account/settings/Index'),
  'BaseSettings': () => import('@/views/account/settings/BaseSetting'),
  'CustomSettings': () => import('@/views/account/settings/Custom'),
  'BindingSettings': () => import('@/views/account/settings/Binding'),
  'welcome': () => import(/* webpackChunkName: "fail" */ '@/views/index/welcome')
}

// 前端未找到页面路由（固定不用改）
const notFoundRouter = {
  path: '*', redirect: '/404', hidden: true
}

const constRouter = [
  {
    url: '/welcome',
    componentName: 'welcome',
    component: 'welcome',
    title: '欢迎',
    hidden: '1'
  }
]

// 根级菜单
const rootRouter = {
  name: 'index',
  url: '/',
  componentName: 'index',
  component: 'BasicLayout',
  redirect: '/welcome',
  title: '首页',
  children: []
}

/**
 * 动态生成菜单
 * @param token
 * @returns {Promise<Router>}
 */
export const generatorDynamicRouter = (code) => {
  return new Promise((resolve, reject) => {
    menuNav(code).then(res => {
      const menuNav = []
      rootRouter.children = res.data.concat(constRouter)
      // 拼接静态网页
      console.log(rootRouter,'rootRouter')
      menuNav.push(rootRouter)
      const routers = generator(menuNav)
      routers.push(notFoundRouter)
      resolve(routers)
    }).catch(err => {
      reject(err)
    })
  })
}

/**
 * 格式化树形结构数据 生成 vue-router 层级路由表
 *
 * @param routerMap
 * @param parent
 * @returns {*}
 */
export const generator = (routerMap) => {
  return routerMap.map(item => {
    console.log(item,item)
    let currentRouter = {}
    if(item.url && (item.url.includes('http') || item.url.includes('https'))){
      const parmas = {
        src: item.url,
        name: item.title
      }
      currentRouter = {
        path: '/myiframe/urlPath',
        parmas: parmas,
        // 路由名称，建议唯一
        name: item.componentName,
        // 该路由对应页面的 组件 :方案1
        // component: constantRouterComponents[item.component || item.key],
        // 该路由对应页面的 组件 :方案2 (动态加载)
        component: () => import('@/components/iframe/main'),
        // meta: 页面标题, 菜单图标, 页面权限(供指令权限用，可去掉)
        meta: {
          title: item.title,
          icon: item.icon || undefined,
          permission: item.componentName
        }
      }
    }else{
      currentRouter = {
        // 如果路由设置了 path，则作为默认 path，否则 路由地址 动态拼接生成如 /dashboard/workplace
        path: item.url,
        // 路由名称，建议唯一
        name: item.componentName,
        // 该路由对应页面的 组件 :方案1
        // component: constantRouterComponents[item.component || item.key],
        // 该路由对应页面的 组件 :方案2 (动态加载)
        component: (constantRouterComponents[item.component]) || (() => import(`@/views/modules/${item.component}`)),
        // meta: 页面标题, 菜单图标, 页面权限(供指令权限用，可去掉)
        meta: {
          title: item.title,
          icon: item.icon || undefined,
          permission: item.componentName
        }
      }
    }
    // 是否设置了隐藏菜单
    if (item.hidden == '1') {
      currentRouter.hidden = item.hidden
    }

    // 重定向
    item.redirect && (currentRouter.redirect = item.redirect)
    // 是否有子菜单，并递归处理
    if (item.children && item.children.length > 0) {
      // Recursion
      currentRouter.children = generator(item.children)
    }
    return currentRouter
  })
}
