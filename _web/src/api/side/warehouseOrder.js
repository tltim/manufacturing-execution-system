import { axios } from '@/utils/request'

const prefix = 'side/order'

export function fetchList (params) {
  return axios({
    url: `/${prefix}/page`,
    method: 'GET',
    params: params
  })
}

export function saveInputOrder(data){
  return axios({
    url: `/${prefix}/saveInputOrder`,
    method: 'POST',
    data
  })
}

export function info(code){
  return axios({
    url: `/${prefix}/info`,
    method: 'GET',
    params: {
      code
    }
  })
}
