import { axios } from '@/utils/request'

const prefix = 'mes/workcenter'

export function fetchList (params) {
  return axios({
    url: `/${prefix}/page`,
    method: 'GET',
    params: params
  })
}

export function addObj (obj) {
  return axios({
    url: `/${prefix}/save`,
    method: 'POST',
    data: obj
  })
}

export function getObj (id) {
  return axios({
    url: `/${prefix}/info/${id}`,
    method: 'GET'
  })
}

export function putObj (obj) {
  return axios({
    url: `/${prefix}/update`,
    method: 'POST',
    data: obj
  })
}

export function delObj (id) {
  return axios({
    url: `/${prefix}/del/${id}`,
    method: 'DELETE'
  })
}

/**
 * 获取工作台数据
 * @param workCenterPlanCode
 * @returns {AxiosPromise}
 */
export function infoWorkPlatform(workCenterPlanCode){
  return axios({
    url: `/${prefix}/workPlanform`,
    method: 'GET',
    params: {
      workCenterPlanCode
    }
  })
}

/**
 * 开始计划
 */
export function release(code){
  return axios({
    url: `/${prefix}/release`,
    method: 'GET',
    params: {
      workCenterPlanCode: code
    }
  })
}
/**
 * 关闭计划
 */
export function close(code){
  return axios({
    url: `/${prefix}/close`,
    method: 'GET',
    params: {
      workCenterPlanCode: code
    }
  })
}

export function work(data){
  return axios({
    url: `/${prefix}/work`,
    method: 'POST',
    data
  })
}

export function feeting(data){
  return axios({
    url: `/${prefix}/feeting`,
    method: 'POST',
    data
  })
}