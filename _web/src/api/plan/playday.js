import { axios } from '@/utils/request'

const prefix = 'mes/planDay'

export function fetchList (params) {
  return axios({
    url: `/${prefix}/page`,
    method: 'GET',
    params: params
  })
}

export function addObj (obj) {
  return axios({
    url: `/${prefix}/save`,
    method: 'POST',
    data: obj
  })
}

export function getObj (id) {
  return axios({
    url: `/${prefix}/info/${id}`,
    method: 'GET'
  })
}

export function putObj (obj) {
  return axios({
    url: `/${prefix}/update`,
    method: 'POST',
    data: obj
  })
}

export function release(params){
  return axios({
    url: `/${prefix}/release`,
    method: 'get',
    params
  })
}

export function delObj (id) {
  return axios({
    url: `/${prefix}/del/${id}`,
    method: 'DELETE'
  })
}

export function stop(planCode){
  return axios({
    url: `/${prefix}/stop`,
    method: 'get',
    params: {
      planCode
    }
  })
}

export function detail (id) {
  return axios({
    url: `/${prefix}/detail`,
    method: 'GET',
    params: {
      id: id
    }
  })
}

export function generateMat(planCode){
  return axios({
    url: `/${prefix}/generateMat`,
    method: 'GET',
    params: {
      planCode: planCode
    }
  })
}

export function check(planCode){
  return axios({
    url: `/${prefix}/check`,
    method: 'GET',
    params: {
      planCode: planCode
    }
  })
}

export function linesPlan(params){
  return axios({
    url: `/${prefix}/linesPlan`,
    method: 'POST',
    data: params
  })
}
