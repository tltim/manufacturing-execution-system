import { axios } from '@/utils/request'

const prefix = 'mes/workreport'

export function fetchList (params) {
  return axios({
    url: `/${prefix}/page`,
    method: 'GET',
    params: params
  })
}