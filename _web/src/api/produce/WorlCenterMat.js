import { axios } from '@/utils/request'

const prefix = 'mes/Mat'

export function fetchList (params) {
  return axios({
    url: `/${prefix}/page`,
    method: 'GET',
    params: params
  })
}

export function returnMaterial(params){
  return axios({
    url: `/${prefix}/returnMat`,
    method: 'GET',
    params: params
  })
}