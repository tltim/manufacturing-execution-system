import { axios } from '@/utils/request'

const prefix = 'mes/product'

export function fetchList (params) {
  return axios({
    url: `/${prefix}/page`,
    method: 'GET',
    params: params
  })
}