import { axios } from '@/utils/request'
import qs from 'qs'



export function loginByTicket(ticket){
  return axios({
    url: '/doLoginByTicket',
    params: {
      ticket
    },
    method: 'GET'
  })
}

export function getInfo () {
  return axios({
    url: '/sys/user/info',
    method: 'get'
  })
}

export function logout () {
  return axios({
    url: '/sso/logout',
    method: 'DELETE',
    params: {
      back: encodeURIComponent(window.location.origin + '/authredirect')
    }
  })
}
/**
 * 未登陆
 */
export function notLogin () {
  const curUrl = encodeURIComponent(window.location.origin + '/authredirect')
  const addr = process.env.VUE_APP_SSO_SERVER + "/sso/auth?redirect=" + curUrl
  top.location.href = addr
}
