import { axios } from '@/utils/request'

const prefix = 'base/bom'

export function fetchList (params) {
  return axios({
    url: `/${prefix}/page`,
    method: 'GET',
    params: params
  })
}

export function saveObj (obj) {
  return axios({
    url: `/${prefix}/save`,
    method: 'POST',
    data: obj
  })
}

export function getObj (id) {
  return axios({
    url: `/${prefix}/info`,
    method: 'GET',
    params:{
      id
    }
  })
}

export function delObj (id) {
  return axios({
    url: `/${prefix}/del/${id}`,
    method: 'DELETE'
  })
}