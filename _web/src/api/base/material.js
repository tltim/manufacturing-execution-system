import { axios } from '@/utils/request'

const prefix = 'base/material'

export function fetchList (params) {
  return axios({
    url: `/${prefix}/page`,
    method: 'GET',
    params: params
  })
}

export function addObj (obj) {
  return axios({
    url: `/${prefix}/save`,
    method: 'POST',
    data: obj
  })
}

export function getObj (params) {
  return axios({
    url: `/${prefix}/info`,
    method: 'GET',
    params
  })
}

export function putObj (obj) {
  return axios({
    url: `/${prefix}/update`,
    method: 'POST',
    data: obj
  })
}

export function delObj (id) {
  return axios({
    url: `/${prefix}/del/${id}`,
    method: 'DELETE'
  })
}

export function analyzeBarcode(barcode){
  return axios({
    url: `/${prefix}/analyze`,
    method: 'GET',
    params: {
      barcode
    }
  })
}