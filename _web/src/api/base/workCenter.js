import { axios } from '@/utils/request'

const prefix = 'base/workcenter'

export function fetchList (params) {
  return axios({
    url: `/${prefix}/page`,
    method: 'GET',
    params: params
  })
}

export function saveObj (obj) {
  return axios({
    url: `/${prefix}/save`,
    method: 'POST',
    data: obj
  })
}

export function getObj (id) {
  return axios({
    url: `/${prefix}/info`,
    method: 'GET',
    params: {
      id
    }
  })
}