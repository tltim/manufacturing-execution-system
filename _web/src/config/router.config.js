// eslint-disable-next-line
import { BasicLayout, BlankLayout, PageView, UserLayout } from '@/layouts'
import { bxAnaalyse } from '@/core/icons'

/**
 * 菜单所需要的字段
 * @type {*[]}
 * path
 * meta : {
 *   title: '首页',
 *   keepAlive: true,
 *   icon: bxAnaalyse
 * }
 * hidden: 可以在菜单中不展示这个路由，包括子路由
 * hideChildrenInMenu: 隐藏不需要在菜单中展示的子路由
 * children: [
 *
 * ]
 *
 */
export const asyncRouterMap = [

  {
    path: '/',
    name: 'index',
    component: BasicLayout,
    meta: { title: '首页' },
    redirect: '/dashboard',
    children: [
      {
        path: '/dashboard',
        name: 'dashboard',
        redirect: '/dashboard/welcome',
        hidden: true,
        component: PageView,
        meta: { title: '仪表盘', keepAlive: true, icon: bxAnaalyse },
        children: [
          {
            path: 'welcome',
            name: 'welcome',
            component: () => import('@/views/index/welcome'),
            meta: { title: '欢迎页面', keepAlive: false }
          },
        ]
      },
      {
        path: '/plan',
        name: 'plan',
        component: PageView,
        meta: { title: '计划管理', keepAlive: true, icon: bxAnaalyse },
        children: [
          {
            path: 'gannt',
            name: 'gannt',
            component: () => import('@/views/main/plan/gannt'),
            meta: { title: '可视化', keepAlive: false }
          },
          {
            path: 'planday',
            name: 'planday',
            component: () => import('@/views/main/plan/planday'),
            meta: { title: '日计划管理', keepAlive: false }
          },
          {
            path: 'workcenterplan',
            name: 'workcenterplan',
            component: () => import('@/views/main/plan/work-center-plan'),
            meta: { title: '工作中心计划', keepAlive: false }
          },
          {
            path: 'workPlatform',
            name: 'workPlatform',
            component: () => import('@/views/main/plan/work-center-plan/WorkPlatform'),
            meta: { title: '报工台', keepAlive: false }
          }
        ]
      },
      {
        path: '/produce',
        name: 'produce',
        component: PageView,
        meta: { title: '生产管理', keepAlive: true, icon: bxAnaalyse },
        children: [
          {
            path: 'workCenterMat',
            name: 'workCenterMat',
            component: () => import('@/views/main/produce/work-center-mat'),
            meta: { title: '生产物料信息', keepAlive: false }
          },
          {
            path: 'workCenterProduct',
            name: 'workCenterProduct',
            component: () => import('@/views/main/produce/work-center-product'),
            meta: { title: '生产产品信息', keepAlive: false }
          }
        ]
      },
      {
        path: '/process',
        name: 'process',
        component: PageView,
        meta: { title: '生产过程记录', keepAlive: true, icon: bxAnaalyse },
        children: [
          {
            path: 'devote',
            name: 'devote',
            component: () => import('@/views/main/process/devote'),
            meta: { title: '投料记录', keepAlive: false }
          },
          {
            path: 'workreportlog',
            name: 'workreportlog',
            component: () => import('@/views/main/process/workreportlog'),
            meta: { title: '报工记录', keepAlive: false }
          },
          {
            path: 'materialRequisition',
            name: 'materialRequisition',
            component: () => import('@/views/main/process/materialRequisition'),
            meta: { title: '领料单', keepAlive: false }
          },
        ]
      },
      {
        path: '/side',
        name: 'side',
        component: PageView,
        meta: { title: '线边管理', keepAlive: true, icon: bxAnaalyse },
        children: [
          {
            path: 'sideSite',
            name: 'sideSite',
            component: () => import('@/views/main/side/sideSite'),
            meta: { title: '库位管理', keepAlive: false }
          },
          {
            path: 'warehouse',
            name: 'warehouse',
            component: () => import('@/views/main/side/warehouse'),
            meta: { title: '线边物料', keepAlive: false }
          },
          {
            path: 'warehouseLog',
            name: 'warehouseLog',
            component: () => import('@/views/main/side/warehouseLog'),
            meta: { title: '出入库日志', keepAlive: false }
          },
          {
            path: 'warehouseOrder',
            name: 'warehouseOrder',
            component: () => import('@/views/main/side/warehouseOrder'),
            meta: { title: '出入库单据', keepAlive: false }
          }
        ]
      },
      {
        path: '/base',
        name: 'base',
        redirect: '/base/',
        component: PageView,
        meta: { title: '基础数据管理', keepAlive: true, icon: bxAnaalyse },
        children: [
          {
            path: 'material',
            name: 'material',
            component: () => import('@/views/main/base/material'),
            meta: { title: '物料管理', keepAlive: false }
          },
          {
            path: 'bom',
            name: 'bom',
            component: () => import('@/views/main/base/bom'),
            meta: { title: 'bom管理', keepAlive: false }
          },
          {
            path: 'lines',
            name: 'lines',
            component: () => import('@/views/main/base/lines'),
            meta: { title: '产线管理', keepAlive: false }
          },
          {
            path: 'workcenter',
            name: 'workcenter',
            component: () => import('@/views/main/base/workcenter'),
            meta: { title: '工作中心', keepAlive: false }
          },
          {
            path: 'device',
            name: 'device',
            component: () => import('@/views/main/base/device'),
            meta: { title: '设备管理', keepAlive: false }
          },
          {
            path: 'station',
            name: 'station',
            component: () => import('@/views/main/base/station'),
            meta: { title: '工位管理', keepAlive: false }
          },
        ]
      },
      // account
      {
        path: '/account',
        component: PageView,
        redirect: '/account/center',
        name: 'account',
        hideChildrenInMenu: true,
        hidden: true,
        meta: { title: '个人页', icon: 'user', keepAlive: true },
        children: [
          {
            path: '/account/settings',
            name: 'settings',
            component: () => import('@/views/account/settings/Index'),
            meta: { title: '个人设置', hideHeader: true },
            redirect: '/account/settings/base',
            children: [
              {
                path: '/account/settings/base',
                name: 'BaseSettings',
                component: () => import('@/views/account/settings/BaseSetting'),
                meta: { title: '基本设置', hidden: true }
              },
              {
                path: '/account/settings/custom',
                name: 'CustomSettings',
                component: () => import('@/views/account/settings/Custom'),
                meta: { title: '个性化设置', hidden: true, keepAlive: true }
              },
              {
                path: '/account/settings/Binding',
                name: 'BindingSettings',
                component: () => import('@/views/account/settings/Binding'),
                meta: { title: '账户绑定', hidden: true, keepAlive: true }
              }
            ]
          }
        ]
      },
    ]
  },
  {
    path: '*', redirect: '/404', hidden: true
  }
]

/**
 * 基础路由
 * @type { *[] }
 */
export const constantRouterMap = [
  {
    path: '/authredirect',
    name: 'authredirect',
    component: () => import('@/page/authredirect')
  },
  {
    path: '/404',
    component: () => import(/* webpackChunkName: "fail" */ '@/views/exception/404')
  }
]
