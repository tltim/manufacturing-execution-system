export const tableObj = {
  columns: [
    {
      title: '工作中心名称',
      field: 'workCenterName',
      type: 'input',
    },
    {
      title: '物料编码',
      field: 'matCode',
      type: 'input',
    },
    {
      title: '物料名称',
      type: 'input',
      field: 'matName'
    },
    {
      title: '物料条码',
      type: 'input',
      sorter: true,
      field: 'matBarcode'
    },
    {
      title: '入库数量',
      field: 'num',
      type: 'input',
    },
    {
      title: '当前数量',
      field: 'currentNumber',
      type: 'date',
    },
    {
      title: '工单编号',
      field: 'planCode',
      type: 'input',
    },
    {
      title: '生产产品名称',
      field: 'productName',
      type: 'input',
    },
    {
      title: '供应商名称',
      field: 'supplierName',
      type: 'input',
    }
  ]
}
