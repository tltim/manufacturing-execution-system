export const tableObj = {
  columns: [
    {
      title: '工作中心计划编码',
      field: 'workCenterPlanCode',
      type: 'input',
    },
    {
      title: '包装物编码',
      field: 'packageCode',
      type: 'input',
      placeholder: '排产编号'
    },
    {
      title: '数量',
      type: 'input',
      field: 'num'
    },
    {
      title: '产品编码',
      field: 'productCode',
      type: 'input',
    },
    {
      title: '产品名称',
      field: 'productName',
      type: 'input',
    },
    {
      title: '工单编码',
      field: 'planCode',
      type: 'input',
    },
    {
      title: '班次',
      field: 'banci',
      type: 'input',
    },
    {
      title: '操作',
      width: '200px',
      hidden: true,
      slot: true,
      field: 'action'
    }
  ]
}
