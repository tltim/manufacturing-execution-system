export const tableObj = {
  columns: [
    {
      title: '库位编码',
      type: 'input',
      field: 'code'
    },
    {
      title: '库位名称',
      type: 'input',
      field: 'name'
    },
    {
      title: '库位长度',
      type: 'input',
      field: 'length'
    },
    {
      title: '库位宽度',
      type: 'input',
      field: 'width'
    },
    {
      title: '操作',
      width: '200px',
      hidden: true,
      slot: true,
      field: 'action'
    }
  ]
}
