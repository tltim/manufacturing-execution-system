export const tableObj = {
  columns: [
    {
      title: '订单编码',
      type: 'input',
      field: 'orderCode'
    },
    {
      title: '出入库类型',
      type: 'input',
      field: 'type',
      options: [
        {
          value: 'INPUT',
          label: '入库'
        },
        {
          value: 'OUTPUT',
          label: '出库'
        }
      ]
    },
    {
      title: '订单状态',
      type: 'input',
      field: 'status'
    },
    {
      title: '出入库原因',
      type: 'input',
      field: 'remark'
    },
    {
      title: '操作',
      width: '200px',
      hidden: true,
      slot: true,
      field: 'action'
    }
  ]
}
