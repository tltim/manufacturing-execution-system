export const tableObj = {
  columns: [
    {
      title: '剩余数量',
      sorter: true,
      type: 'input',
      field: 'currentNumber'
    },
    {
      title: '物料编码',
      type: 'input',
      field: 'matCode'
    },
    {
      title: '物料名称',
      type: 'input',
      field: 'matName'
    },
    {
      title: '物料条码',
      type: 'input',
      field: 'matBarcode'
    },
    {
      title: '供应商编码',
      type: 'input',
      field: 'supplier'
    },
    {
      title: '物料生产批次',
      type: 'input',
      field: 'matBatch'
    },
    {
      title: '最早入库时间',
      sorter: true,
      type: 'input',
      field: 'enterTime'
    },
    {
      title: '操作',
      width: '200px',
      hidden: true,
      slot: true,
      field: 'action'
    }
  ]
}
