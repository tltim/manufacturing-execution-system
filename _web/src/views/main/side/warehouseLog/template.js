export const tableObj = {
  columns: [
    {
      title: '物料编码',
      type: 'input',
      field: 'matCode'
    },
    {
      title: '物料名称',
      type: 'input',
      field: 'matName'
    },
    {
      title: '物料条码',
      type: 'input',
      field: 'matBarcode'
    },
    {
      title: '供应商编码',
      type: 'input',
      field: 'supplier'
    },
    {
      title: '物料生产批次',
      type: 'input',
      field: 'num'
    },
    {
      title: '箱码',
      type: 'input',
      field: 'boxCode'
    },
    {
      title: '类型',
      type: 'dict',
      field: 'type',
      options: [
        {
          value: 'INPUT',
          label: '入库'
        },
        {
          value: 'OUTPUT',
          label: '出库'
        }
      ]
    },
    {
      title: '出入库订单号',
      type: 'input',
      field: 'orderCode'
    }
    // {
    //   title: '操作',
    //   width: '200px',
    //   hidden: true,
    //   slot: true,
    //   field: 'action'
    // }
  ]
}
