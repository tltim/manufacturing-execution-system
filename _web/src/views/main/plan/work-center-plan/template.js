export const tableObj = {
  columns: [
    {
      title: '排产编号',
      field: 'planCode',
      type: 'input',
      placeholder: '排产编号'
    },
    {
      title: '产品编号',
      type: 'input',
      field: 'productCode'
    },
    {
      title: '产品名称',
      field: 'productName',
      type: 'input',
    },
    {
      title: '计划数量',
      type: 'input',
      sorter: true,
      field: 'productCount'
    },
    {
      title: '完成数量',
      type: 'input',
      sorter: true,
      field: 'successCount'
    },
    {
      title: '工作中心名称',
      field: 'name',
      type: 'input',
    },
    {
      title: '工位名称',
      field: 'stationName',
      type: 'input',
    },
    {
      title: '状态',
      field: 'status',
      type: 'input',
    },
    {
      title: '操作',
      width: '200px',
      hidden: true,
      slot: true,
      field: 'action'
    }
  ]
}
