export const workCenter = {
  columns: [
    {
      title: '排产编号',
      field: 'planCode',
      type: 'input',
      placeholder: '排产编号'
    },
    {
      title: '产品编号',
      type: 'input',
      field: 'productCode'
    },
    {
      title: '产品名称',
      field: 'productName',
      type: 'input',
    },
    {
      title: '计划数量',
      type: 'input',
      sorter: true,
      field: 'productCount'
    },
    {
      title: '完成数量',
      type: 'input',
      sorter: true,
      field: 'successCount'
    },
    {
      title: '工作中心名称',
      field: 'name',
      type: 'input',
    },
    {
      title: '工位名称',
      field: 'stationName',
      type: 'input',
    },
    {
      title: '状态',
      field: 'status',
      type: 'input',
    }
  ]
}

export const devoteMat = {
  columns: [
    {
      title: '排产编号',
      field: 'planCode',
      type: 'input',
    },
    {
      title: '工作中心编码',
      field: 'workCenterCode',
      type: 'input',
    },
    {
      title: '物料编码',
      type: 'input',
      field: 'matCode'
    },
    {
      title: '物料名称',
      field: 'matName',
      type: 'input',
    },
    {
      title: '物料条码',
      field: 'matBarcode',
      type: 'input',
    },
    {
      title: '产品编号',
      type: 'input',
      field: 'productCode'
    },
    {
      title: '产品名称',
      field: 'productName',
      type: 'input',
    },
    {
      title: '数量',
      type: 'input',
      sorter: true,
      field: 'num'
    }
  ]
}

