export const tableObj = {
  columns: [
    {
      title: '合同订单编号',
      field: 'orderId',
      type: 'input',
      placeholder: '合同订单编号'
    },
    {
      title: '排产编号',
      field: 'planCode',
      type: 'input',
      placeholder: '排产编号'
    },
    {
      title: '产品编号',
      type: 'input',
      field: 'productCode'
    },
    {
      title: '计划数量',
      type: 'input',
      sorter: true,
      slot: true,
      field: 'productCount'
    },
    {
      title: '完成数量',
      type: 'input',
      sorter: true,
      field: 'successCount'
    },
    {
      title: '产品名称',
      field: 'productName',
      type: 'input',
    },
    {
      title: '产线编码',
      field: 'linesCode',
      type: 'input',
    },
    {
      title: '产线名称',
      field: 'linesName',
      type: 'input',
    },
    {
      title: '状态',
      field: 'status',
      type: 'input',
    },
    {
      title: '计划开始时间',
      field: 'planStartDate',
      type: 'date',
    },
    {
      title: '计划结束时间',
      field: 'planEndDate',
      hidden: true,
      type: 'date',
    },
    {
      title: '产能',
      field: 'productSpeed',
      type: 'input',
      hidden: true,
    },
    {
      title: '操作',
      width: '300',
      hidden: true,
      slot: true,
      field: 'action'
    }
  ]
}
