export const tableObj = {
  columns: [
    {
      title: '排产编号',
      field: 'planCode',
      type: 'input',
    },
    {
      title: '产品编号',
      type: 'input',
      field: 'productCode'
    },
    {
      title: '产品名称',
      field: 'productName',
      type: 'input',
    },
    {
      title: '物料编码',
      type: 'input',
      field: 'matCode'
    },
    {
      title: '物料名称',
      field: 'matName',
      type: 'input',
    },
    {
      title: '数量',
      type: 'input',
      sorter: true,
      field: 'num'
    },
    {
      title: '班次',
      field: 'banci',
      type: 'input',
    },
    {
      title: '配送时间',
      field: 'deliveryTime',
      type: 'date',
    },
  ]
}