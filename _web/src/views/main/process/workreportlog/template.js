export const tableObj = {
  columns: [
    {
      title: '排产编号',
      field: 'planCode',
      type: 'input',
    },
    {
      title: '工作中心编码',
      field: 'workCenterCode',
      type: 'input',
    },
    {
      title: '产品编码',
      type: 'input',
      field: 'matCode'
    },
    {
      title: '产品名称',
      field: 'matName',
      type: 'input',
    },
    {
      title: '数量',
      field: 'num',
      type: 'input',
    },
    {
      title: '箱码/包装物吗',
      type: 'input',
      field: 'boxCode'
    },
    {
      title: '报工时间',
      field: 'createdTime',
      type: 'date',
    },
    {
      title: '报工员工',
      type: 'input',
      field: 'createdBy'
    }

  ]
}