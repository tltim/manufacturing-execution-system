export const tableObj = {
  columns: [
    {
      title: '物料编码',
      sorter: true,
      type: 'input',
      field: 'matCode'
    },
    {
      title: '物料名称',
      sorter: true,
      type: 'input',
      field: 'matName'
    },
    {
      title: '物料类型',
      sorter: true,
      type: 'input',
      field: 'matType'
    },
    {
      title: '来源 采购/自制',
      sorter: true,
      type: 'input',
      field: 'source'
    },
    {
      title: '单位',
      sorter: true,
      type: 'input',
      field: 'unit'
    }
    // {
    //   title: '操作',
    //   width: '200px',
    //   hidden: true,
    //   slot: true,
    //   field: 'action'
    // }
  ]
}
