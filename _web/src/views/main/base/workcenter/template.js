export const tableObj = {
  columns: [
    {
      title: '产线编号',
      sorter: true,
      type: 'input',
      field: 'lineCode'
    },
    {
      title: '工作中心编码',
      type: 'input',
      field: 'code'
    },
    {
      title: '工作中心名称',
      type: 'input',
      field: 'name'
    },
    {
      title: '设备编码',
      type: 'input',
      field: 'deviceCode'
    },
    {
      title: '设备名称',
      type: 'input',
      field: 'deviceName'
    },
    {
      title: '工位编码',
      type: 'input',
      field: 'stationCode'
    },
    {
      title: '工位名称',
      type: 'input',
      field: 'stationName'
    },
    {
      title: '输入物料编码',
      type: 'input',
      field: 'inputMats'
    },
    {
      title: '工作模式',
      type: 'dict',
      field: 'mode',
      options: [
        {
          label: '投料',
          value: 'input'
        },
        {
          label: '报工',
          value: 'output'
        }
      ]
    },
    {
      title: '报工模式',
      type: 'dict',
      field: 'type',
      options: [
        {
          label: '半品',
          value: 'semi'
        },
        {
          label: '成品',
          value: 'finish'
        }
      ]
    },
    {
      title: '操作',
      width: '200px',
      hidden: true,
      slot: true,
      field: 'action'
    }
  ]
}
