export const tableObj = {
  columns: [
    {
      title: '产线编号',
      sorter: true,
      type: 'input',
      field: 'lineCode'
    },
    {
      title: '产线名称',
      type: 'input',
      field: 'lineName'
    },
    {
      title: '物料编号',
      type: 'input',
      field: 'matCode'
    },
    {
      title: '物料名称',
      type: 'input',
      field: 'matName'
    },
    {
      title: '产能',
      sorter: true,
      type: 'input',
      field: 'capacity'
    },
    {
      title: '产线状态',
      sorter: true,
      type: 'input',
      field: 'status'
    },
    {
      title: '操作',
      width: '200px',
      hidden: true,
      slot: true,
      field: 'action'
    }
  ]
}
