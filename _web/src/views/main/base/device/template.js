export const tableObj = {
  columns: [
    {
      title: '设备编号',
      sorter: true,
      type: 'input',
      field: 'code'
    },
    {
      title: '设备描述',
      type: 'input',
      field: 'content'
    },
    {
      title: '设备名称',
      type: 'input',
      field: 'name'
    },
    {
      title: '设备状态',
      type: 'input',
      field: 'status'
    },
    {
      title: '设备类型',
      type: 'input',
      field: 'type'
    },
    {
      title: '操作',
      width: '200px',
      hidden: true,
      slot: true,
      field: 'action'
    }
  ]
}
