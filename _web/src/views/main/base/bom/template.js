export const tableObj = {
  columns: [
    {
      title: '父级物料编码',
      sorter: true,
      type: 'input',
      field: 'parentCode'
    },
    {
      title: '父级物料名称',
      type: 'input',
      field: 'parentName'
    },
    {
      title: '物料编码',
      type: 'input',
      field: 'matCode'
    },
    {
      title: '物料名称',
      type: 'input',
      field: 'matName'
    },
    {
      title: '每件需求量',
      type: 'input',
      field: 'count'
    },
    {
      title: '单位',
      type: 'input',
      field: 'unit'
    },
    {
      title: '操作',
      width: '200px',
      hidden: true,
      slot: true,
      field: 'action'
    }
  ]
}
