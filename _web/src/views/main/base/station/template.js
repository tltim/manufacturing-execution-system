export const tableObj = {
  columns: [
    {
      title: '工位编码',
      sorter: true,
      type: 'input',
      field: 'code'
    },
    {
      title: '工位名称',
      type: 'input',
      field: 'name'
    },
    {
      title: '工位状态',
      type: 'input',
      field: 'status'
    },
    {
      title: '产线编码',
      type: 'input',
      field: 'lineCode'
    },
    {
      title: '产线名称',
      type: 'input',
      field: 'lineName'
    },
    {
      title: '操作',
      width: '200px',
      hidden: true,
      slot: true,
      field: 'action'
    }
  ]
}
