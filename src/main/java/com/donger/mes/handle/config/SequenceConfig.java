package com.donger.mes.handle.config;

import cn.hutool.core.date.DateUtil;
import com.donger.common.sequence.builder.DbSeqBuilder;
import com.donger.common.sequence.properties.SequenceDbProperties;
import com.donger.common.sequence.sequence.Sequence;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class SequenceConfig {



    /**
     * 工单生成
     * @param dataSource
     * @param properties
     * @return
     */
    @Bean
    public Sequence planCodeSequence(DataSource dataSource,
                                     SequenceDbProperties properties){
        return DbSeqBuilder
                .create()
                .bizName(() -> String.format("plan_%s", DateUtil.today()))
                .dataSource(dataSource)
                .prefix(() -> "PL_")
                .step(properties.getStep())
                .retryTimes(properties.getRetryTimes())
                .tableName(properties.getTableName())
                .build();
    }

    /**
     * 工单生成
     * @param dataSource
     * @param properties
     * @return
     */
    @Bean
    public Sequence workCenterCodeSequence(DataSource dataSource,
                                     SequenceDbProperties properties){
        return DbSeqBuilder
                .create()
                .bizName(() -> String.format("plan_%s", DateUtil.today()))
                .dataSource(dataSource)
                .prefix(() -> "WPL_")
                .step(properties.getStep())
                .retryTimes(properties.getRetryTimes())
                .tableName(properties.getTableName())
                .build();
    }
}
