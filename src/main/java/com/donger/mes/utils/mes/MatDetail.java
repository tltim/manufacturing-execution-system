package com.donger.mes.utils.mes;

import lombok.Data;

/**
 * 物料详情
 */

@Data
public class MatDetail {
    private String oldBarcode;
    private String matCode;
    private String matName;
    private String supplier;
    private float num;
    private String batch;
    private String barcode;
    private Boolean isBox = false;
    private Boolean isStandard = true;
    private String boxId;
    private String matType;
}
