package com.donger.mes.utils.mes;

import cn.hutool.extra.spring.SpringUtil;
import com.donger.common.sequence.sequence.Sequence;
import com.donger.mes.system.base.entity.MesBaseMaterial;
import com.donger.mes.system.base.service.MesBaseMaterialService;
import com.donger.mes.system.mes.enums.MesCodeEnums;
import com.donger.common.core.utils.BizException;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

public class MesUtils {


    /**
     *
     * @param startTime 开始时间
     * @param time 产能
     * @param total 生产数量
     * @return
     */
    public static LocalDateTime calcTimeByProductSpeed(LocalDateTime startTime,Long time,Long total){

        // 计算得出分钟数 按照分钟进行计算
        long minutes = total * 60 / time;

        LocalDateTime localDateTime = startTime.plusMinutes(minutes);
        return localDateTime;
    }

    /**
     * 根据开始时间来计算日计划的班次
     * @param startTime
     * @return
     */
    public static String calcBanciByDate(LocalDateTime startTime){

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyMMdd");
        int hour = startTime.getHour();

        String banci = "";

        LocalDate date = LocalDate.of(startTime.getYear(), startTime.getMonth(), startTime.getDayOfMonth());
        // 白班
        if (hour >= 8 && hour < 20) {
            banci = date.format(dateTimeFormatter) + "A";
        } else if (hour < 8) {
            // 昨天的夜班
            LocalDate plus = date.plusDays(-1);
            banci = plus.format(dateTimeFormatter) + "B";
        } else {
            // 今天的夜班
            banci = date.format(dateTimeFormatter) + "B";
        }
        return banci;
    }

    public static String calcBanciByDate(){
        return MesUtils.calcBanciByDate(LocalDateTime.now());
    }


    /**
     * 生成排产单号
     * @return
     */
    public static String getMesCode(MesCodeEnums codeEnums){

        if(codeEnums.equals(MesCodeEnums.PLANCODE)){
            LocalDateTime now = LocalDateTime.now();
            Sequence planCodeSequence = SpringUtil.getBean("planCodeSequence",Sequence.class);
            return planCodeSequence.nextNo();
        }else if(codeEnums.equals(MesCodeEnums.WORKCENTERPLANCODE)){
            Sequence workCenterCodeSequence = SpringUtil.getBean("workCenterCodeSequence",Sequence.class);
            return workCenterCodeSequence.nextNo();
        }else if(codeEnums.equals(MesCodeEnums.SIDEORDERCODE)){
            LocalDateTime now = LocalDateTime.now();
            String yyyymmddhHmmss = now.format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
            return "SOR" +yyyymmddhHmmss;
        }
        return "";
    }


    /**
     * 解析物料条码
     * 四段式条码，活这三段式条码进行截断
     * @param matBarcode
     * @return
     */
    public static MatDetail MatBarcodeSplit(String matBarcode){

        MatDetail detail = new MatDetail();
        detail.setOldBarcode(matBarcode);
        String[] split = matBarcode.split("/");
        MesBaseMaterialService mesBaseMaterialService = SpringUtil.getBean(MesBaseMaterialService.class);



        if(split.length == 4 || split.length == 3){
            detail.setSupplier(split[0]);
            detail.setMatCode(split[1]);
            detail.setBatch(split[2]);
            detail.setBarcode(split[0] + '/' + split[1] + '/' + split[2]);
            if(split.length == 4){
                detail.setNum(Float.parseFloat(split[3]));
            }
            MesBaseMaterial mesBaseMaterial = Optional.ofNullable(mesBaseMaterialService.infoByCode(detail.getMatCode()))
                    .orElseThrow(() -> new BizException("不存在该物料"));
            detail.setMatName(mesBaseMaterial.getMatName());
        }else{
            throw new BizException("物料条码无法识别");
        }





        return detail;
    }
}
