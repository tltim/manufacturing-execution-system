package com.donger.mes.utils.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gitee.sunchenbin.mybatis.actable.annotation.ColumnComment;
import lombok.Data;

import java.time.LocalDateTime;


@Data
public class BaseEntity {

    /**
     * 自动递增ID
     */
    @TableId
    private Long id;

    @ColumnComment("创建人")
    @TableField(fill = FieldFill.INSERT)
    private String createdBy;
    @ColumnComment("创建时间")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createdTime;
    @ColumnComment("更新人")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updatedBy;

    @ColumnComment("更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updatedTime;
}
