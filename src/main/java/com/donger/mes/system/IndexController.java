package com.donger.mes.system;

import cn.hutool.extra.spring.SpringUtil;
import com.donger.common.core.utils.Res;
import com.donger.common.core.utils.Result;
import com.donger.common.sso.entity.UserInfo;
import com.donger.common.sso.feign.SsoUserFeign;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.donger.common.sso.utils.AuthUtils.getUserId;

/**
 * @author pmc
 */
@RequestMapping("/")
@RestController
public class IndexController {

    @RequestMapping("/")
    public String index() {
        return "index";
    }

    @GetMapping("/info")
    public Result<UserInfo> info() {
//        UserInfo userInfo = AuthUtils.getUserInfo();

        Long userId = getUserId();
        SsoUserFeign ssoUserFeign = SpringUtil.getBean(SsoUserFeign.class);
        Result<UserInfo> userInfo = ssoUserFeign.getUserInfo(userId);
        UserInfo data = userInfo.getData();
//        CACHE_MAP.put(String.valueOf(userId), data);


        return Res.ok(data);
    }
}
