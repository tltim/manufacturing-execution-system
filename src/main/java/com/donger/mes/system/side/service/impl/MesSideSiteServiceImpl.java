package com.donger.mes.system.side.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.donger.mes.system.side.entity.MesSideSite;
import com.donger.mes.system.side.mapper.MesSideSiteMapper;
import com.donger.mes.system.side.service.MesSideSiteService;
import org.springframework.stereotype.Service;

@Service
public class MesSideSiteServiceImpl extends ServiceImpl<MesSideSiteMapper, MesSideSite> implements MesSideSiteService {
}
