package com.donger.mes.system.side.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.donger.mes.utils.entity.BaseEntity;
import com.gitee.sunchenbin.mybatis.actable.annotation.ColumnComment;
import com.gitee.sunchenbin.mybatis.actable.annotation.DefaultValue;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import lombok.Data;

@TableComment("库位")
@TableName
@Data
public class MesSideSite extends BaseEntity {

    /**
     * 库位编码
     */
    @ColumnComment("库位编码")
    @TableField
    private String code;

    /**
     * 库位名称
     */
    @ColumnComment("库位编码")
    @TableField
    private String name;


    /**
     * 库位长度 注意单位
     */
    @ColumnComment("库位长度")
    @TableField
    private Integer length;


    /**
     * 库位宽度 注意单位
     */
    @ColumnComment("库位宽度")
    @TableField
    private Integer width;


    /**
     * 是否已删除  0未删除，1已删除
     */
    @ColumnComment("是否已删除")
    @TableField
    @DefaultValue("0")
    private Integer delFlag;

}
