package com.donger.mes.system.side.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.donger.mes.system.side.entity.MesSideWarehouseLog;
import com.donger.mes.system.side.mapper.MesSideWarehouseLogMapper;
import com.donger.mes.system.side.service.MesSideWarehouseLogService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class MesSideWarehouseLogServiceImpl implements MesSideWarehouseLogService {
    private final MesSideWarehouseLogMapper mesSideWarehouseLogMapper;

    @Override
    public IPage<MesSideWarehouseLog> page(Page<MesSideWarehouseLog> page, Wrapper<MesSideWarehouseLog> queryWrapper) {
        return mesSideWarehouseLogMapper.selectPage(page, queryWrapper);
    }
}
