package com.donger.mes.system.side.controller;


import com.donger.mes.system.side.entity.MesSideSite;
import com.donger.mes.system.side.service.MesSideSiteService;
import com.donger.mes.utils.entity.BaseController;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 库位管理
 */
@RequestMapping("/side/site")
@RestController
@AllArgsConstructor
public class MesSideSiteController extends BaseController<MesSideSiteService, MesSideSite> {


}
