package com.donger.mes.system.side.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.donger.mes.system.side.entity.MesSideWarehouse;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MesSideWarehouseMapper extends BaseMapper<MesSideWarehouse> {
}
