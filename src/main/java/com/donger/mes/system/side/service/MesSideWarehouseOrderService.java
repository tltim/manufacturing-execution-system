package com.donger.mes.system.side.service;


import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.donger.mes.system.side.dto.MesSideWarehouseOrderDTO;
import com.donger.mes.system.side.entity.MesSideWarehouseOrder;

public interface MesSideWarehouseOrderService {

    IPage<MesSideWarehouseOrder> page(Page<MesSideWarehouseOrder> page, Wrapper<MesSideWarehouseOrder> queryWrapper);

    void saveInputOrder(MesSideWarehouseOrderDTO mesSideWarehouseOrderDTO);

    void saveOutputOrder(MesSideWarehouseOrderDTO mesSideWarehouseOrderDTO);

    MesSideWarehouseOrderDTO getOrderByCode(String orderCode);
}
