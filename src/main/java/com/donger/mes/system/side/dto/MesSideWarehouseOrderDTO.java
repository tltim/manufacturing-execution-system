package com.donger.mes.system.side.dto;

import com.donger.mes.system.side.entity.MesSideWarehouseOrderItem;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
public class MesSideWarehouseOrderDTO {


    private String orderCode;


    @NotEmpty(message = "业务类型不能为空")
    private String businessType;


    private String remark;
    @NotEmpty(message = "订单类型不能为空")
    private String type;

    private List<MesSideWarehouseOrderItemDTO> items;
}
