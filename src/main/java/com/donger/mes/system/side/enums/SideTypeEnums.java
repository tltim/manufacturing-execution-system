package com.donger.mes.system.side.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum SideTypeEnums {


    INPUT("INOUT","入库"),  //入库
    OUTPUT("OUTPUT","出库");  // 出库


    /**
     * 类型
     */
    private String type;
    /**
     * 描述
     */
    private String description;

}
