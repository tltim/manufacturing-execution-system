package com.donger.mes.system.side.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.donger.mes.system.side.entity.MesSideWarehouseOrderItem;
import com.donger.mes.system.side.mapper.MesSideWarehouseOrderItemMapper;
import com.donger.mes.system.side.service.MesSideWarehouseOrderItemService;
import org.springframework.stereotype.Service;

@Service
public class MesSideWarehouseOrderItemServiceImpl extends ServiceImpl<MesSideWarehouseOrderItemMapper, MesSideWarehouseOrderItem> implements MesSideWarehouseOrderItemService {
}
