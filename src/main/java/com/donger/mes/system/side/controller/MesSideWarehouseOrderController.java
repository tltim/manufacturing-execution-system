package com.donger.mes.system.side.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.donger.mes.system.side.dto.MesSideWarehouseOrderDTO;
import com.donger.mes.system.side.entity.MesSideWarehouseOrder;
import com.donger.mes.system.side.enums.SideTypeEnums;
import com.donger.mes.system.side.service.MesSideWarehouseOrderService;
import com.donger.common.core.utils.Res;
import com.donger.common.core.utils.Result;
import com.donger.common.core.utils.query.enums.CommonQuery;
import com.donger.common.core.utils.query.utils.QueryGenerator;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/side/order")
@RestController
@AllArgsConstructor
public class MesSideWarehouseOrderController {

    private final MesSideWarehouseOrderService mesSideWarehouseOrderService;


    /**
     * 分页查询订单
     *
     * @param page
     * @param order
     * @param query
     * @return
     */
    @GetMapping("/page")
    public Result page(Page<MesSideWarehouseOrder> page, MesSideWarehouseOrder order, CommonQuery query) {
        QueryWrapper<MesSideWarehouseOrder> queryWrapper = QueryGenerator.initQueryWrapper(order, query);
        mesSideWarehouseOrderService.page(page, queryWrapper);
        return Res.ok(page);
    }


    /**
     * 新增入库单
     *
     * @param mesSideWarehouseOrderDTO
     * @return
     */
    @PostMapping("/saveInputOrder")
    public Result saveInputOrder(@RequestBody MesSideWarehouseOrderDTO mesSideWarehouseOrderDTO) {

        // 校验必须的数据
        mesSideWarehouseOrderDTO.setType(SideTypeEnums.INPUT.getType());
        mesSideWarehouseOrderService.saveInputOrder(mesSideWarehouseOrderDTO);
        return Res.ok();
    }


    /**
     * 新增入库单
     *
     * @param mesSideWarehouseOrderDTO
     * @return
     */
    @PostMapping("/saveOutputOrder")
    public Result saveOutputOrder(@RequestBody MesSideWarehouseOrderDTO mesSideWarehouseOrderDTO) {

        // 校验必须的数据
        mesSideWarehouseOrderDTO.setType(SideTypeEnums.OUTPUT.getType());
        mesSideWarehouseOrderService.saveOutputOrder(mesSideWarehouseOrderDTO);
        return Res.ok();
    }


    /**
     * 获取订单详情
     * @param code
     * @return
     */
    @GetMapping("/info")
    public Result info(String code){
        MesSideWarehouseOrderDTO orderDTO = mesSideWarehouseOrderService.getOrderByCode(code);
        return Res.ok(orderDTO);
    }
}
