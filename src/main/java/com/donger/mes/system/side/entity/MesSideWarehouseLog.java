package com.donger.mes.system.side.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.donger.mes.utils.entity.BaseEntity;
import com.gitee.sunchenbin.mybatis.actable.annotation.ColumnComment;
import com.gitee.sunchenbin.mybatis.actable.annotation.DefaultValue;
import com.gitee.sunchenbin.mybatis.actable.annotation.IsNotNull;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 出入库日志
 */
@TableName
@Data
@TableComment("出入库日志")
public class MesSideWarehouseLog extends BaseEntity {

    @TableField
    @ColumnComment("物料编码")
    @IsNotNull
    private String matCode;
    @TableField
    @ColumnComment("物料名称")
    @IsNotNull
    private String matName;
    @TableField
    @ColumnComment("物料条码的前三个字段")
    @IsNotNull
    private String matBarcode;

    @TableField
    @ColumnComment("供应商编码")
    private String supplier;

    @TableField
    @ColumnComment("数量")
    @DefaultValue("0")
    private Float num;

    @TableField
    @ColumnComment("库位编码")
    private String siteCode;

    /**
     * 箱码
     */
    @TableField
    @ColumnComment("箱码")
    private String boxCode;


    @TableField
    @ColumnComment("出入库类型  入库 input 出库 output")
    private String type;


    @TableField
    @ColumnComment("业务类型 采购入库")
    private String bussinessType;


    @TableField
    @ColumnComment("出入库订单号")
    private String orderCode;
}

