package com.donger.mes.system.side.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.donger.mes.system.side.entity.MesSideWarehouse;
import com.donger.mes.system.side.entity.MesSideWarehouseOrderItem;
import com.donger.mes.system.side.mapper.MesSideWarehouseOrderItemMapper;

public interface MesSideWarehouseOrderItemService extends IService<MesSideWarehouseOrderItem> {
}
