package com.donger.mes.system.side.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.gitee.sunchenbin.mybatis.actable.annotation.ColumnComment;
import com.gitee.sunchenbin.mybatis.actable.annotation.DefaultValue;
import com.gitee.sunchenbin.mybatis.actable.annotation.IsNotNull;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class MesSideWarehouseOrderItemDTO {


    @NotEmpty(message = "物料编码不能为空")
    private String matCode;


    @NotEmpty(message = "物料名称不能为空")
    private String matName;


    @NotEmpty(message = "物料条码不能为空")
    private String matBarcode;



    @NotEmpty(message = "供应商信息不能为空")
    private String supplier;




    @NotEmpty(message = "数量不能为空")
    private Float num;




    private String boxCode;
}
