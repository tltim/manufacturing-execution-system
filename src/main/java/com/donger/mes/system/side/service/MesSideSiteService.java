package com.donger.mes.system.side.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.donger.mes.system.side.entity.MesSideSite;

public interface MesSideSiteService extends IService<MesSideSite> {
}
