package com.donger.mes.system.side.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.donger.mes.system.side.entity.MesSideWarehouseLog;

public interface MesSideWarehouseLogService {
    IPage<MesSideWarehouseLog> page(Page<MesSideWarehouseLog> page, Wrapper<MesSideWarehouseLog> queryWrapper);
}
