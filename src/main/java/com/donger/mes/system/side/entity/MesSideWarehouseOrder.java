package com.donger.mes.system.side.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.donger.mes.utils.entity.BaseEntity;
import com.gitee.sunchenbin.mybatis.actable.annotation.ColumnComment;
import com.gitee.sunchenbin.mybatis.actable.annotation.IsNotNull;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import lombok.Data;

/**
 * 出入库订单
 */
@TableName
@TableComment("出入库订单")
@Data
public class MesSideWarehouseOrder extends BaseEntity {


    @TableField
    @ColumnComment("订单编码")
    @IsNotNull
    private String orderCode;


    @TableField
    @ColumnComment("出入库类型")
    @IsNotNull
    private String type;


    @TableField
    @ColumnComment("业务类型")
    @IsNotNull
    private String businessType;


    @TableField
    @ColumnComment("出入库原因")
    private String remark;

    @TableField
    @ColumnComment("订单状态")
    private String status;


}
