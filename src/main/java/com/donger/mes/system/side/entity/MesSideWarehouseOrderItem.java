package com.donger.mes.system.side.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.donger.mes.utils.entity.BaseEntity;
import com.gitee.sunchenbin.mybatis.actable.annotation.ColumnComment;
import com.gitee.sunchenbin.mybatis.actable.annotation.DefaultValue;
import com.gitee.sunchenbin.mybatis.actable.annotation.IsNotNull;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import lombok.Data;

/**
 * 订单详情
 */
@TableName
@TableComment("订单详情")
@Data
public class MesSideWarehouseOrderItem extends BaseEntity {


    @TableField
    @ColumnComment("订单编码")
    @IsNotNull
    private String orderCode;

    @TableField
    @ColumnComment("物料编码")
    @IsNotNull
    private String matCode;
    @TableField
    @ColumnComment("物料名称")
    @IsNotNull
    private String matName;
    @TableField
    @ColumnComment("物料条码的前三个字段")
    @IsNotNull
    private String matBarcode;

    @TableField
    @ColumnComment("供应商编码")
    private String supplier;


    @TableField
    @ColumnComment("数量")
    @DefaultValue("0")
    private Float num;

    /**
     * 箱码
     */
    @TableField
    @ColumnComment("箱码")
    private String boxCode;

}
