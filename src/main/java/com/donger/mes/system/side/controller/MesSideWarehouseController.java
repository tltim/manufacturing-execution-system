package com.donger.mes.system.side.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.donger.mes.system.side.entity.MesSideWarehouse;
import com.donger.mes.system.side.service.MesSideWarehouseService;
import com.donger.common.core.utils.Res;
import com.donger.common.core.utils.Result;
import com.donger.common.core.utils.query.enums.CommonQuery;
import com.donger.common.core.utils.query.utils.QueryGenerator;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 线边库
 */
@RequestMapping("/side")
@RestController
@AllArgsConstructor
public class MesSideWarehouseController {
    private final MesSideWarehouseService mesSideWarehouseService;


    /**
     * 分页查询
     * @param page
     * @param mesSideWarehouse
     * @param query
     * @return
     */
    @GetMapping("/page")
    public Result page(Page<MesSideWarehouse> page, MesSideWarehouse mesSideWarehouse, CommonQuery query){
        QueryWrapper<MesSideWarehouse> queryWrapper = QueryGenerator.<MesSideWarehouse>initQueryWrapper(mesSideWarehouse, query);
        mesSideWarehouseService.page(page,queryWrapper);
        return Res.ok(page);
    }


    /**
     * 入库单执行入库
     * @param orderCode
     * @return
     */
    @PostMapping("/input")
    public Result input(String orderCode){
        mesSideWarehouseService.input(orderCode);


        return Res.ok();
    }


    /**
     * 出库单执行出库
     * @param orderCode
     * @return
     */
    @PostMapping("/output")
    public Result output(String orderCode){
        mesSideWarehouseService.output(orderCode);


        return Res.ok();
    }
}
