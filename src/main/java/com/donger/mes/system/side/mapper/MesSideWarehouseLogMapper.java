package com.donger.mes.system.side.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.donger.mes.system.side.entity.MesSideWarehouseLog;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MesSideWarehouseLogMapper extends BaseMapper<MesSideWarehouseLog> {
}
