package com.donger.mes.system.side.dto;

import lombok.Data;

@Data
public class MaterialReportVO {
    private String matCode;
    private String matName;
    private Float num;
    private String site;
}
