package com.donger.mes.system.side.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.donger.mes.system.side.dto.MaterialReportVO;
import com.donger.mes.system.side.entity.MesSideWarehouse;

import java.util.List;

public interface MesSideWarehouseService {

    IPage<MesSideWarehouse> page(IPage<MesSideWarehouse> page, Wrapper<MesSideWarehouse> query);


    /**
     * 物料条码
     *
     * @param barcode 物料条码
     * @param num 数量
     * @param type 类型 暂时没用
     * @return
     */
    MesSideWarehouse output(String barcode, Float num,String type,String siteCode);


    MesSideWarehouse output(String barcode, Float num, String type,String siteCode,String orderCode);


    /**
     * 按照订单号进行出库
     * @param orderCode
     * @return
     */
    List<MesSideWarehouse> output(String orderCode);


    /**
     * 入库
     * @param mesSideWarehouse
     */
    void input(MesSideWarehouse mesSideWarehouse);

    void input(MesSideWarehouse mesSideWarehouse,String orderCode);


    /**
     * 按照订单号进行入库
     * @param orderCode
     */
    void input(String orderCode);


    /**
     * 多种物料
     * @param matCodes
     * @return
     */
    List<MaterialReportVO> reportMatNum(List<String> matCodes);
}
