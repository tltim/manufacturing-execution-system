package com.donger.mes.system.side.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.donger.mes.system.side.entity.MesSideWarehouseLog;
import com.donger.mes.system.side.service.MesSideWarehouseLogService;
import com.donger.common.core.utils.Res;
import com.donger.common.core.utils.Result;
import com.donger.common.core.utils.query.enums.CommonQuery;
import com.donger.common.core.utils.query.utils.QueryGenerator;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/side/log")
@RestController
@AllArgsConstructor
public class MesSideWarehouseLogController {
    private final MesSideWarehouseLogService mesSideWarehouseLogService;


    /**
     * 分页查询日志
     *
     * @param page
     * @param log
     * @param query
     * @return
     */
    @GetMapping("/page")
    public Result page(Page<MesSideWarehouseLog> page, MesSideWarehouseLog log, CommonQuery query) {
        QueryWrapper<MesSideWarehouseLog> queryWrapper = QueryGenerator.initQueryWrapper(log, query);
        mesSideWarehouseLogService.page(page, queryWrapper);
        return Res.ok(page);
    }
}
