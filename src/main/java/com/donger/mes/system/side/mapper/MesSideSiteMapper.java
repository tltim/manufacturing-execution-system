package com.donger.mes.system.side.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.donger.mes.system.side.entity.MesSideSite;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MesSideSiteMapper extends BaseMapper<MesSideSite> {
}
