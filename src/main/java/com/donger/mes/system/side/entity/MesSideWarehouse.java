package com.donger.mes.system.side.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.Version;
import com.donger.mes.utils.entity.BaseEntity;
import com.gitee.sunchenbin.mybatis.actable.annotation.ColumnComment;
import com.gitee.sunchenbin.mybatis.actable.annotation.DefaultValue;
import com.gitee.sunchenbin.mybatis.actable.annotation.IsNotNull;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName
@TableComment("线边库")
public class MesSideWarehouse extends BaseEntity {


    /**
     * 当前数量
     */
    @TableField
    @ColumnComment("当前数量")
    @IsNotNull
    @DefaultValue("0")
    private Float currentNumber;
    /**
     * 物料编码
     */
    @TableField
    @ColumnComment("物料编码")
    @IsNotNull
    private String matCode;

    /**
     * 物料名称
     */
    @TableField
    @ColumnComment("物料名称")
    @IsNotNull
    private String matName;


    /**
     * 物料条码的前三个字段
     */
    @TableField
    @ColumnComment("物料条码的前三个字段")
    @IsNotNull
    private String matBarcode;


    @TableField
    @ColumnComment("供应商编码")
    private String supplier;


    /**
     * 物料生产批次
     */
    @TableField
    @ColumnComment("物料生产批次")
    private String matBatch;


    /**
     * 最早入库时间
     */
    @TableField
    @ColumnComment("最早入库时间")
    private LocalDateTime enterTime;

    /**
     * 是否冻结 0未冻结 1已冻结
     */
    @TableField
    @ColumnComment("是否冻结")
    private String isFrozen;


    /**
     * 箱码
     */
    @TableField
    @ColumnComment("箱码")
    private String boxCode;

    @TableField
    @ColumnComment("版本")
    @Version
    @DefaultValue("0")
    private Long version;



    /**
     * 状态 是否被锁定
     */
    @TableField
    @ColumnComment("状态")
    private Integer status;


    /**
     * 单位
     */
    @TableField
    @ColumnComment("单位")
    private Integer unit;


    /**
     * 库位编码
     */
    @TableField
    @ColumnComment("库位编码")
    private String siteCode;


}
