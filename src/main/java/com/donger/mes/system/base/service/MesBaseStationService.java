package com.donger.mes.system.base.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.donger.mes.system.base.entity.MesBaseStation;

public interface MesBaseStationService {
    IPage<MesBaseStation> page(IPage<MesBaseStation> page, QueryWrapper<MesBaseStation> queryWrapper);

    MesBaseStation infoById(Long id);

    void save(MesBaseStation mesBaseStation);

    MesBaseStation infoByCode(String code);

}
