package com.donger.mes.system.base.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.donger.common.core.utils.Res;
import com.donger.common.core.utils.Result;
import com.donger.common.core.utils.query.enums.CommonQuery;
import com.donger.common.core.utils.query.utils.QueryGenerator;
import com.donger.mes.system.base.entity.MesBaseDevice;
import com.donger.mes.system.base.service.MesBaseDeviceService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/base/device")
@RestController
@AllArgsConstructor
public class MesBaseDeviceController {
    private final MesBaseDeviceService mesBaseDeviceService;


    @GetMapping("/page")
    public Result page(Page<MesBaseDevice> page, MesBaseDevice queryEntity, CommonQuery commonQuery) {
        QueryWrapper<MesBaseDevice> queryWrapper = QueryGenerator.initQueryWrapper(queryEntity, commonQuery);
        mesBaseDeviceService.page(page, queryWrapper);
        return Res.ok(page);
    }


    @GetMapping("/info")
    public Result info(Long id) {

        MesBaseDevice mesBaseDevice = mesBaseDeviceService.info(id);

        return Res.ok(mesBaseDevice);
    }


    @GetMapping("/infoByCode")
    public Result infoByCode(String code) {

        MesBaseDevice mesBaseDevice = mesBaseDeviceService.infoByColumn(Wrappers.<MesBaseDevice>lambdaQuery()
                .eq(MesBaseDevice::getCode, code)
        );

        return Res.ok(mesBaseDevice);
    }


    /**
     * 保存数据
     *
     * @param mesBaseDevice
     * @return
     */
    @PostMapping("/save")
    public Result save(@RequestBody MesBaseDevice mesBaseDevice) {
        mesBaseDeviceService.save(mesBaseDevice);
        return Res.ok();

    }


    /**
     * 删除
     *
     * @param id
     * @return
     */
    @DeleteMapping("/delete")
    public Result delete(Long id) {
        mesBaseDeviceService.deleteById(id);
        return Res.ok();
    }
}
