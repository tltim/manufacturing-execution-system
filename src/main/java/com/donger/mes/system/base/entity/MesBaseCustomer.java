package com.donger.mes.system.base.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.donger.mes.utils.entity.BaseEntity;
import com.gitee.sunchenbin.mybatis.actable.annotation.ColumnComment;
import com.gitee.sunchenbin.mybatis.actable.annotation.IsNotNull;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 客户信息表 客户信息表
 *
 * @author pmc
 */
@Data
@TableName
@TableComment("客户信息表")
public class MesBaseCustomer extends BaseEntity implements Serializable {

    private static final long serialVersionUID = -1;

    @ColumnComment("客户名称")
    @TableField
    @IsNotNull
    private String customerName;

    @ColumnComment("客户编码")
    @TableField
    @IsNotNull
    private String customerCode;

    @ColumnComment("地址")
    @TableField
    private String address;

    @ColumnComment("物料编码")
    @TableField
    private String matCode;

    @ColumnComment("物料名称")
    @TableField
    private String matName;

    @ColumnComment("本地仓库")
    @TableField
    private String sodLoc;

    @ColumnComment("三方仓库")
    @TableField
    private String sodConsignLoc;
}