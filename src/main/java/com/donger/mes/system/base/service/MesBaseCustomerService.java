package com.donger.mes.system.base.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.donger.mes.system.base.entity.MesBaseCustomer;

public interface MesBaseCustomerService {

    IPage<MesBaseCustomer> page(IPage<MesBaseCustomer> page, QueryWrapper<MesBaseCustomer> queryWrapper);
}
