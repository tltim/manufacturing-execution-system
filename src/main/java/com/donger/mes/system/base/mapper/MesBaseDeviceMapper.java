package com.donger.mes.system.base.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.donger.mes.system.base.entity.MesBaseDevice;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author pmc
 */
@Mapper
public interface MesBaseDeviceMapper extends BaseMapper<MesBaseDevice> {
}
