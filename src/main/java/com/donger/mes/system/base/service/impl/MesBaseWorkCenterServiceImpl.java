package com.donger.mes.system.base.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.donger.mes.system.base.entity.*;
import com.donger.mes.system.base.mapper.MesBaseDeviceMapper;
import com.donger.mes.system.base.mapper.MesBaseLineMapper;
import com.donger.mes.system.base.mapper.MesBaseWorkCenterMapper;
import com.donger.mes.system.base.service.*;
import com.donger.common.core.utils.BizException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class MesBaseWorkCenterServiceImpl implements MesBaseWorkCenterService {

    private final MesBaseWorkCenterMapper mesBaseWorkCenterMapper;

    private final MesBaseLineService mesBaseLineService;



    private final MesBaseDeviceService mesBaseDeviceService;

    private final MesBaseMaterialService mesBaseMaterialService;

    private final MesBaseStationService mesBaseStationService;




    /**
     * 根据产线查询对应的工作中心
     * @param lineCode 产线编码
     * @return
     */
    @Override
    public List<MesBaseWorkCenter> getListByLineCode(String lineCode) {
        List<MesBaseWorkCenter> workCenterList = mesBaseWorkCenterMapper.selectList(Wrappers.<MesBaseWorkCenter>lambdaQuery()
                .eq(MesBaseWorkCenter::getLineCode, lineCode));
        return workCenterList;
    }

    /**
     * 工作中心
     * @param page
     * @param queryWrapper
     * @return
     */
    @Override
    public IPage<MesBaseWorkCenter> page(IPage<MesBaseWorkCenter> page, QueryWrapper<MesBaseWorkCenter> queryWrapper) {
        return mesBaseWorkCenterMapper.selectPage(page,queryWrapper);
    }

    @Override
    public MesBaseWorkCenter info(Long id) {
        return this.infoByColumn(Wrappers.<MesBaseWorkCenter>lambdaQuery().eq(MesBaseWorkCenter::getId,id));
    }

    @Override
    public MesBaseWorkCenter infoByColumn(Wrapper<MesBaseWorkCenter> queryWrapper) {
        return mesBaseWorkCenterMapper.selectOne(queryWrapper);
    }


    /**
     * 保存工作中心
     * 校验产线数据
     * 校验设备数据
     * 校验工位数据 （ 暂时没有
     * 校验输入输出物料数据 （ 物料基础信息中获取填写的物料编码是否都正确
     * 校验报工模式
     *
     *
     * @param mesBaseWorkCenter 工作中心
     * @return
     */
    @Override
    public Boolean save(MesBaseWorkCenter mesBaseWorkCenter) {


        String lineCode = mesBaseWorkCenter.getLineCode();

        Optional.ofNullable(mesBaseLineService.infoByCode(lineCode)).orElseThrow(() -> new BizException("设备信息不存在"));


        String deviceCode = mesBaseWorkCenter.getDeviceCode();

        MesBaseDevice mesBaseDevice = Optional.ofNullable(mesBaseDeviceService.infoByCode(deviceCode)).orElseThrow(() -> new BizException("设备信息不存在"));
        mesBaseWorkCenter.setDeviceName(mesBaseDevice.getName());

        String stationCode = Optional.ofNullable(mesBaseWorkCenter.getStationCode()).orElseThrow(() -> new BizException("工位不能为空"));

        MesBaseStation mesBaseStation = mesBaseStationService.infoByCode(stationCode);
        mesBaseWorkCenter.setStationName(mesBaseStation.getName());


        if("output".equals(mesBaseWorkCenter.getMode())){
            Optional.ofNullable(mesBaseWorkCenter.getOutputType()).orElseThrow(() -> new BizException("报工类型不能为空"));

            Optional.ofNullable(mesBaseWorkCenter.getBackflushMode()).orElseThrow(() -> new BizException("倒冲模式不能为空"));
        }



        if(mesBaseWorkCenter.getId() == null){
            mesBaseWorkCenterMapper.insert(mesBaseWorkCenter);
        }else{
            mesBaseWorkCenterMapper.updateById(mesBaseWorkCenter);
        }

        return Boolean.TRUE;
    }
}
