package com.donger.mes.system.base.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.donger.mes.utils.entity.BaseEntity;
import com.gitee.sunchenbin.mybatis.actable.annotation.ColumnComment;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import lombok.Data;
import org.springframework.cloud.openfeign.CollectionFormat;

@Data
@TableName
@TableComment("工作中心")
public class MesBaseWorkCenter extends BaseEntity {


    @TableField
    @ColumnComment("产线编码")
    private String lineCode;

    @TableField
    @ColumnComment("工作中心编码")
    private String code;
    @TableField
    @ColumnComment("工作中心名称")
    private String name;

    @TableField
    @ColumnComment("设备编码")
    private String deviceCode;

    @TableField
    @ColumnComment("设备名称")
    private String deviceName;

    @TableField
    @ColumnComment("工位编码")
    private String stationCode;

    @TableField
    @ColumnComment("工位名称")
    private String stationName;

    @TableField
    @ColumnComment("输入物料编码")
    private String inputMats;

    @TableField
    @ColumnComment("排序")
    private Long orderNum;


    /**
     * input
     * output
     */
    @TableField
    @ColumnComment("工作模式 投料  报工")
    private String mode;


    /**
     *  {
     *    title: '半品',
     *    value: 'semi'
     *   },
     *   {
     *    title: '成品',
     *    value: 'finish'
     *   }
     */
    @TableField
    @ColumnComment("报工模式")
    private String type;


    /**
     * 单码报工
     * 合并报工
     */
    @TableField
    @ColumnComment("报工类型")
    private String outputType;


    /**
     * bom 按照bom倒冲
     * input  按照输入类型倒冲
     */
    @TableField
    @ColumnComment("倒冲模式")
    private String backflushMode;




}
