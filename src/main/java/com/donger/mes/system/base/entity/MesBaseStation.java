package com.donger.mes.system.base.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.donger.mes.utils.entity.BaseEntity;
import com.gitee.sunchenbin.mybatis.actable.annotation.ColumnComment;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import lombok.Data;

@Data
@TableName
@TableComment("基础工位表")
public class MesBaseStation extends BaseEntity {
    @TableField
    @ColumnComment("工位编码")
    private String code;
    @TableField
    @ColumnComment("工位名称")
    private String name;
    @TableField
    @ColumnComment("产线编码")
    private String lineCode;
    @TableField
    @ColumnComment("产线名称")
    private String lineName;
    @TableField
    @ColumnComment("状态")
    private String status;
}
