package com.donger.mes.system.base.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.donger.mes.system.base.entity.MesBaseLine;
import com.donger.mes.system.base.service.MesBaseLineService;
import com.donger.common.core.utils.Res;
import com.donger.common.core.utils.Result;
import com.donger.common.core.utils.query.enums.CommonQuery;
import com.donger.common.core.utils.query.utils.QueryGenerator;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/base/line")
@RestController
@AllArgsConstructor
public class MesBaseLineController {

    private final MesBaseLineService mesBaseLineService;

    /**
     * 分页查询工单数据
     *
     * @return
     */
    @GetMapping("/page")
    public Result page(Page<MesBaseLine> page, MesBaseLine MesBaseLine, CommonQuery commonQuery) {
        QueryWrapper<MesBaseLine> queryWrapper = QueryGenerator.initQueryWrapper(MesBaseLine, commonQuery);
        mesBaseLineService.page(page, queryWrapper);
        return Res.ok(page);
    }


    @GetMapping("/info")
    public Result info(String lineCode) {
        MesBaseLine one = mesBaseLineService.infoByCode(lineCode);
        return Res.ok(one);
    }


    @PostMapping("/save")
    public Result save(@RequestBody MesBaseLine mesBaseLine){
        mesBaseLineService.save(mesBaseLine);

        return Res.ok();
    }
}
