package com.donger.mes.system.base.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.donger.mes.system.base.entity.SysConfig;

public interface SysConfigService extends IService<SysConfig> {
}
