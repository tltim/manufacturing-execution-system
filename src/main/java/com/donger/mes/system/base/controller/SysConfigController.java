package com.donger.mes.system.base.controller;


import com.donger.mes.system.base.entity.SysConfig;
import com.donger.mes.system.base.service.SysConfigService;
import com.donger.mes.utils.entity.BaseController;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 系统配置
 */
@RequestMapping("/base/config")
@RestController
@AllArgsConstructor
public class SysConfigController extends BaseController<SysConfigService, SysConfig> {
}
