package com.donger.mes.system.base.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.donger.mes.system.base.entity.MesBaseMaterial;

public interface MesBaseMaterialService extends IService<MesBaseMaterial> {

    MesBaseMaterial infoByCode(String matCode);
}
