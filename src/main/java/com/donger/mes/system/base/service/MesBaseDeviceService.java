package com.donger.mes.system.base.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.donger.mes.system.base.entity.MesBaseDevice;

public interface MesBaseDeviceService {


    IPage<MesBaseDevice> page(IPage<MesBaseDevice> page, QueryWrapper<MesBaseDevice> queryWrapper);

    MesBaseDevice info(Long id);


    MesBaseDevice infoByCode(String code);


    MesBaseDevice infoByColumn(Wrapper<MesBaseDevice> queryWrapper);

    Boolean save(MesBaseDevice mesBaseDevice);

    void deleteById(Long id);
}
