package com.donger.mes.system.base.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.donger.mes.system.base.entity.MesBaseLine;

public interface MesBaseLineService {

    IPage<MesBaseLine> page(IPage<MesBaseLine> page, Wrapper<MesBaseLine> queryWrapper);


    MesBaseLine infoByCode(String lineCode);


    MesBaseLine info(Long id);

    void save(MesBaseLine mesBaseLine);

}
