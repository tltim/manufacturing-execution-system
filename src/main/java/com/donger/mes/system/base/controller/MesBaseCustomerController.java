package com.donger.mes.system.base.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.donger.mes.system.base.entity.MesBaseBom;
import com.donger.mes.system.base.entity.MesBaseCustomer;
import com.donger.mes.system.base.service.MesBaseCustomerService;
import com.donger.common.core.utils.Res;
import com.donger.common.core.utils.Result;
import com.donger.common.core.utils.query.enums.CommonQuery;
import com.donger.common.core.utils.query.utils.QueryGenerator;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/base/customer")
@RestController
@AllArgsConstructor
public class MesBaseCustomerController {

    private final MesBaseCustomerService mesBaseCustomerService;


    /**
     * 分页查询
     * @param page
     * @param queryEntity
     * @param commonQuery
     * @return
     */
    @GetMapping("/page")
    public Result page(Page<MesBaseCustomer> page, MesBaseCustomer queryEntity, CommonQuery commonQuery) {
        QueryWrapper<MesBaseCustomer> queryWrapper = QueryGenerator.initQueryWrapper(queryEntity, commonQuery);
        mesBaseCustomerService.page(page, queryWrapper);
        return Res.ok(page);
    }
}
