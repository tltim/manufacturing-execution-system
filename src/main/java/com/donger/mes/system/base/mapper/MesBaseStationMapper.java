package com.donger.mes.system.base.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.donger.mes.system.base.entity.MesBaseStation;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MesBaseStationMapper extends BaseMapper<MesBaseStation> {
}
