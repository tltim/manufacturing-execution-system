package com.donger.mes.system.base.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.donger.mes.utils.entity.BaseEntity;
import com.gitee.sunchenbin.mybatis.actable.annotation.ColumnComment;
import com.gitee.sunchenbin.mybatis.actable.annotation.IsNotNull;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import lombok.Data;

import java.io.Serializable;

/**
 * mes-基础-物料
 *
 * @author pmc
 */
@Data
@TableName
@TableComment("基础物料表")
public class MesBaseMaterial extends BaseEntity implements Serializable {

    private static final long serialVersionUID = -1;


    @ColumnComment("物料编码")
    @TableField
    @IsNotNull
    private String matCode;

    @ColumnComment("物料名称")
    @TableField
    @IsNotNull
    private String matName;

    /**
     * 0产品，1，半成品，2 原辅料
     */
    @ColumnComment("物料类型： 0产品，1，半成品，2 原辅料")
    @TableField
    private String matType;
    /**
     * 来源 采购/自制
     */
    @ColumnComment("来源 采购/自制")
    @TableField
    private String source;
    /**
     * 是否吨散包
     */
    @ColumnComment("是否吨散包")
    @TableField
    private Integer packFlag;

    /**
     * 最大安全库存
     */
    @ColumnComment("最大安全库存")
    @TableField
    private Integer maxSafeStock;

    /**
     * 最小安全库存
     */
    @ColumnComment("最小安全库存")
    @TableField
    private Integer minSafeStock;

    /**
     * 单位
     */
    @ColumnComment("单位")
    @TableField
    private String unit;



}