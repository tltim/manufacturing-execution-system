package com.donger.mes.system.base.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.donger.mes.utils.entity.BaseEntity;
import com.gitee.sunchenbin.mybatis.actable.annotation.ColumnComment;
import com.gitee.sunchenbin.mybatis.actable.annotation.DefaultValue;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import lombok.Data;

/**
 * 系统配置 各种功能开关配置，已经自动生成部分参数配置
 */
@Data
@TableName
@TableComment("系统配置")
public class SysConfig extends BaseEntity {

    @TableField
    @ColumnComment("配置编码")
    private String code;
    @TableField
    @ColumnComment("配置值")
    private String value;

    @TableField
    @ColumnComment("配置说明")
    private String label;

    @TableField
    @ColumnComment("是否系统默认:0系统默认，不能删除 1用户配置")
    @DefaultValue("1")
    private String isSystem;


    @TableField
    @ColumnComment("配置类型")
    private String type;
}
