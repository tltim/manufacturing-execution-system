package com.donger.mes.system.base.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.donger.mes.system.base.entity.MesBaseDevice;
import com.donger.mes.system.base.mapper.MesBaseDeviceMapper;
import com.donger.mes.system.base.service.MesBaseDeviceService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class MesBaseDeviceServiceImpl implements MesBaseDeviceService {
    private final MesBaseDeviceMapper mesBaseDeviceMapper;

    @Override
    public IPage<MesBaseDevice> page(IPage<MesBaseDevice> page, QueryWrapper<MesBaseDevice> queryWrapper) {
        return mesBaseDeviceMapper.selectPage(page, queryWrapper);
    }

    @Override
    public MesBaseDevice info(Long id) {
        return mesBaseDeviceMapper.selectById(id);
    }

    @Override
    public MesBaseDevice infoByCode(String code) {
        return this.infoByColumn(Wrappers.<MesBaseDevice>lambdaQuery().eq(MesBaseDevice::getCode,code));
    }

    @Override
    public MesBaseDevice infoByColumn(Wrapper<MesBaseDevice> queryWrapper) {
        return mesBaseDeviceMapper.selectOne(queryWrapper);
    }

    @Override
    public Boolean save(MesBaseDevice mesBaseDevice) {
        int flag = 0;
        if(mesBaseDevice.getId() == null){
            flag = mesBaseDeviceMapper.insert(mesBaseDevice);
        }else{
            flag =  mesBaseDeviceMapper.updateById(mesBaseDevice);
        }
        return flag == 1;
    }

    @Override
    public void deleteById(Long id) {
        mesBaseDeviceMapper.deleteById(id);
    }


}
