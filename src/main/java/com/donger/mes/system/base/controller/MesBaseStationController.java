package com.donger.mes.system.base.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.donger.mes.system.base.entity.MesBaseStation;
import com.donger.mes.system.base.service.MesBaseStationService;
import com.donger.common.core.utils.Res;
import com.donger.common.core.utils.Result;
import com.donger.common.core.utils.query.enums.CommonQuery;
import com.donger.common.core.utils.query.utils.QueryGenerator;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 * 工位管理
 */
@RequestMapping("/base/station")
@RestController
@AllArgsConstructor
public class MesBaseStationController {
    private final MesBaseStationService mesBaseStationService;


    /**
     * 分页查询工位
     * @param page
     * @param eneity
     * @param query
     * @return
     */
    @GetMapping("/page")
    public Result page(Page<MesBaseStation> page, MesBaseStation eneity, CommonQuery query){
        QueryWrapper<MesBaseStation> queryWrapper = QueryGenerator.<MesBaseStation>initQueryWrapper(eneity, query);
        mesBaseStationService.page(page,queryWrapper);
        return Res.ok(page);
    }


    /**
     * 查询详情
     * @param id
     * @return
     */
    @GetMapping("/info")
    public Result info(Long id){
        MesBaseStation mesBaseStation = mesBaseStationService.infoById(id);
        return Res.ok(mesBaseStation);
    }


    /**
     * 查询详情
     * @param code
     * @return
     */
    @GetMapping("/infoByCode")
    public Result infoByCode(String code){
        MesBaseStation mesBaseStation = mesBaseStationService.infoByCode(code);
        return Res.ok(mesBaseStation);
    }

    /**
     * 保存工位
     * @param mesBaseStation
     * @return
     */
    @PostMapping("/save")
    public Result save(@RequestBody MesBaseStation mesBaseStation){

        mesBaseStationService.save(mesBaseStation);
        return Res.ok();
    }
}
