package com.donger.mes.system.base.service;


import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.donger.mes.system.base.entity.MesBaseDevice;
import com.donger.mes.system.base.entity.MesBaseWorkCenter;
import com.donger.mes.system.mes.entity.WorkCenterProduct;

import java.util.List;

/**
 * 工作中心逻辑
 */
public interface MesBaseWorkCenterService {


    List<MesBaseWorkCenter> getListByLineCode(String lineCode);

    IPage<MesBaseWorkCenter> page(IPage<MesBaseWorkCenter> page, QueryWrapper<MesBaseWorkCenter> queryWrapper);

    MesBaseWorkCenter info(Long id);

    MesBaseWorkCenter infoByColumn(Wrapper<MesBaseWorkCenter> queryWrapper);

    Boolean save(MesBaseWorkCenter mesBaseWorkCenter);
}
