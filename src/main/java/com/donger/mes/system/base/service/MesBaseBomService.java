package com.donger.mes.system.base.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.donger.mes.system.base.entity.MesBaseBom;

import java.util.List;

public interface MesBaseBomService {

    IPage<MesBaseBom> page(IPage<MesBaseBom> page, QueryWrapper<MesBaseBom> queryWrapper);

    void save(MesBaseBom mesBaseBom);

    MesBaseBom infoById(Long id);


    List<MesBaseBom> list(String parentCode);
}
