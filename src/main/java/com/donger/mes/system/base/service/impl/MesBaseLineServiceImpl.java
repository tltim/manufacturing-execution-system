package com.donger.mes.system.base.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.donger.mes.system.base.entity.MesBaseLine;
import com.donger.mes.system.base.entity.MesBaseMaterial;
import com.donger.mes.system.base.mapper.MesBaseLineMapper;
import com.donger.mes.system.base.mapper.MesBaseMaterialMapper;
import com.donger.mes.system.base.service.MesBaseLineService;
import com.donger.mes.system.base.service.MesBaseMaterialService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class MesBaseLineServiceImpl implements MesBaseLineService {
    private final MesBaseLineMapper mesBaseLineMapper;
    private final MesBaseMaterialService mesBaseMaterialService;


    @Override
    public IPage<MesBaseLine> page(IPage<MesBaseLine> page, Wrapper<MesBaseLine> queryWrapper) {
        return mesBaseLineMapper.selectPage(page,queryWrapper);
    }

    @Override
    public MesBaseLine infoByCode(String lineCode) {
        return mesBaseLineMapper.selectOne(Wrappers.<MesBaseLine>lambdaQuery().eq(MesBaseLine::getLineCode,lineCode));
    }

    @Override
    public MesBaseLine info(Long id) {
        return mesBaseLineMapper.selectById(id);
    }

    @Override
    public void save(MesBaseLine mesBaseLine) {

        // 新增是校验是否存在重复数据

        String matCode = mesBaseLine.getMatCode();
        MesBaseMaterial mesBaseMaterial = mesBaseMaterialService.infoByCode(matCode);
        mesBaseLine.setMatName(mesBaseMaterial.getMatName());


        //
        if(mesBaseLine.getId() == null){
            mesBaseLineMapper.insert(mesBaseLine);
        }else{
            mesBaseLineMapper.updateById(mesBaseLine);
        }
    }
}
