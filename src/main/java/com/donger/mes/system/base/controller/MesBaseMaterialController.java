package com.donger.mes.system.base.controller;


import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.donger.mes.system.base.entity.MesBaseMaterial;
import com.donger.mes.system.base.service.MesBaseMaterialService;
import com.donger.common.core.utils.Res;
import com.donger.common.core.utils.Result;
import com.donger.mes.utils.mes.MatDetail;
import com.donger.mes.utils.mes.MesUtils;
import com.donger.common.core.utils.query.enums.CommonQuery;
import com.donger.common.core.utils.query.utils.QueryGenerator;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/base/material")
@RestController
@AllArgsConstructor
public class MesBaseMaterialController {

    private final MesBaseMaterialService mesBaseMaterialService;

    /**
     * 分页查询工单数据
     *
     * @return
     */
    @GetMapping("/page")
    public Result page(Page<MesBaseMaterial> page, MesBaseMaterial mesBaseMaterial, CommonQuery commonQuery) {
        QueryWrapper<MesBaseMaterial> queryWrapper = QueryGenerator.initQueryWrapper(mesBaseMaterial, commonQuery);
        mesBaseMaterialService.page(page, queryWrapper);
        return Res.ok(page);
    }


    @GetMapping("/info")
    public Result info(String code){
        MesBaseMaterial one = mesBaseMaterialService.getOne(Wrappers.<MesBaseMaterial>lambdaQuery()
                .eq(MesBaseMaterial::getMatCode, code), false
        );
        return Res.ok(one);
    }


    /**
     * 解析物料条码
     * @param barcode
     * @return
     */
    @GetMapping("/analyze")
    public Result analyzeBarcode(String barcode){

        MatDetail matDetail = MesUtils.MatBarcodeSplit(barcode);


        return Res.ok(matDetail);
    }
}
