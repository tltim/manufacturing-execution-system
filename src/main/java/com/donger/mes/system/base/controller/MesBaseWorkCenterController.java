package com.donger.mes.system.base.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.donger.mes.system.base.entity.MesBaseWorkCenter;
import com.donger.mes.system.base.service.MesBaseWorkCenterService;
import com.donger.common.core.utils.Res;
import com.donger.common.core.utils.Result;
import com.donger.common.core.utils.query.enums.CommonQuery;
import com.donger.common.core.utils.query.utils.QueryGenerator;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 * 工作中心
 */
@RequestMapping("/base/workcenter")
@RestController
@AllArgsConstructor
public class MesBaseWorkCenterController {

    public final MesBaseWorkCenterService mesBaseWorkCenterService;


    /**
     * 分页查询
     *
     * @param page
     * @param mesBaseWorkCenter
     * @param commonQuery
     * @return
     */
    @GetMapping("/page")
    public Result page(Page<MesBaseWorkCenter> page, MesBaseWorkCenter mesBaseWorkCenter, CommonQuery commonQuery) {
        QueryWrapper<MesBaseWorkCenter> queryWrapper = QueryGenerator.initQueryWrapper(mesBaseWorkCenter, commonQuery);
        mesBaseWorkCenterService.page(page, queryWrapper);
        return Res.ok(page);
    }


    @GetMapping("/info")
    public Result info(Long id){

        MesBaseWorkCenter mesBaseWorkCenter = mesBaseWorkCenterService.info(id);

        return Res.ok(mesBaseWorkCenter);
    }



    @PostMapping("/save")
    public Result save(@RequestBody MesBaseWorkCenter mesBaseWorkCenter){

        Boolean bool = mesBaseWorkCenterService.save(mesBaseWorkCenter);

        return Res.ok();
    }





}
