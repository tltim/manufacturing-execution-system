package com.donger.mes.system.base.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.donger.common.core.utils.Res;
import com.donger.common.core.utils.Result;
import com.donger.common.core.utils.query.enums.CommonQuery;
import com.donger.common.core.utils.query.utils.QueryGenerator;
import com.donger.mes.system.base.entity.MesBaseBom;
import com.donger.mes.system.base.service.MesBaseBomService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * bom
 */
@RequestMapping("/base/bom")
@RestController
@AllArgsConstructor
public class MesBaseBomController {

    private final MesBaseBomService mesBaseBomService;


    /**
     * 分页查询
     *
     * @param page
     * @param queryEntity
     * @param commonQuery
     * @return
     */
    @GetMapping("/page")
    public Result page(Page<MesBaseBom> page, MesBaseBom queryEntity, CommonQuery commonQuery) {
        QueryWrapper<MesBaseBom> queryWrapper = QueryGenerator.initQueryWrapper(queryEntity, commonQuery);
        mesBaseBomService.page(page, queryWrapper);
        return Res.ok(page);
    }


    @PostMapping("/save")
    public Result save(@RequestBody MesBaseBom mesBaseBom) {

        mesBaseBomService.save(mesBaseBom);

        return Res.ok();
    }


    @GetMapping("/info")
    public Result info(Long id) {
        MesBaseBom mesBaseBom = mesBaseBomService.infoById(id);
        return Res.ok(mesBaseBom);
    }


}
