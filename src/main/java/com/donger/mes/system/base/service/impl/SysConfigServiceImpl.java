package com.donger.mes.system.base.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.donger.mes.system.base.entity.SysConfig;
import com.donger.mes.system.base.mapper.SysConfigMapper;
import com.donger.mes.system.base.service.SysConfigService;
import org.springframework.stereotype.Service;

@Service
public class SysConfigServiceImpl extends ServiceImpl<SysConfigMapper, SysConfig> implements SysConfigService {
}
