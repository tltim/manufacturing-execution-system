package com.donger.mes.system.base.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.donger.mes.utils.entity.BaseEntity;
import com.gitee.sunchenbin.mybatis.actable.annotation.ColumnComment;
import com.gitee.sunchenbin.mybatis.actable.annotation.IsNotNull;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 物料BOM
 * @author pmc
 */
@Data
@TableName
@TableComment("物料BOM")
public class MesBaseBom extends BaseEntity implements Serializable {

    private static final long serialVersionUID = -1; 


    @ColumnComment("物料编码")
    @TableField
    @IsNotNull
    private String matCode;

    @ColumnComment("物料名称")
    @TableField
    @IsNotNull
    private String matName;

    @ColumnComment("父级物料编码")
    @TableField
    private String parentCode;

    @ColumnComment("父级物料名称")
    @TableField
    private String parentName;
        
    @ColumnComment("每件需求量")
    @TableField
    private Float count;
        
    @ColumnComment("单位")
    @TableField
    private String unit;
        
    @ColumnComment("物料编码")
    @TableField
    private String judge;
}