package com.donger.mes.system.base.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.donger.mes.system.base.entity.MesBaseBom;
import com.donger.mes.system.base.entity.MesBaseMaterial;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author pmc
 */
@Mapper
public interface MesBaseMaterialMapper extends BaseMapper<MesBaseMaterial> {
}
