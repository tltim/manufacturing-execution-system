package com.donger.mes.system.base.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.donger.mes.system.base.entity.MesBaseCustomer;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MesBaseCustomerMapper extends BaseMapper<MesBaseCustomer> {
}
