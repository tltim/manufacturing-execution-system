package com.donger.mes.system.base.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.donger.mes.system.base.entity.MesBaseCustomer;
import com.donger.mes.system.base.mapper.MesBaseCustomerMapper;
import com.donger.mes.system.base.service.MesBaseCustomerService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class MesBaseCustomerServiceImpl implements MesBaseCustomerService {
    private final MesBaseCustomerMapper mesBaseCustomerMapper;

    @Override
    public IPage<MesBaseCustomer> page(IPage<MesBaseCustomer> page, QueryWrapper<MesBaseCustomer> queryWrapper) {
        return mesBaseCustomerMapper.selectPage(page,queryWrapper);
    }
}
