package com.donger.mes.system.base.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.donger.mes.utils.entity.BaseEntity;
import com.gitee.sunchenbin.mybatis.actable.annotation.ColumnComment;
import com.gitee.sunchenbin.mybatis.actable.annotation.DefaultValue;
import com.gitee.sunchenbin.mybatis.actable.annotation.IsNotNull;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import lombok.Data;

import java.io.Serializable;

/**
 * MES-基础-设备表 (MesBaseDevice)表实体类
 *
 * @author pmc
 */
@Data
@TableName
@TableComment("设备表")
public class MesBaseDevice extends BaseEntity implements Serializable {

    private static final long serialVersionUID = -1; 



    /** 上级设备 */
    @ColumnComment("上级设备")
    @TableField
    @IsNotNull
    @DefaultValue("0")
    private Long parentId;
        
    /**设备编号*/
    @ColumnComment("设备编号")
    @TableField
    @IsNotNull
    private String code;
        
    /**设备描述*/
    @ColumnComment("设备描述")
    @TableField
    @IsNotNull
    private String content;
        
    /**设备名称*/
    @ColumnComment("设备名称")
    @TableField
    @IsNotNull
    private String name;
        
    /**设备状态*/
    @ColumnComment("设备状态")
    @TableField
    @IsNotNull
    private String status;

    /**设备类型 1：注塑；2：装配*/
    @ColumnComment("设备类型 1：注塑；2：装配")
    @TableField
    @IsNotNull
    private Long type;
}