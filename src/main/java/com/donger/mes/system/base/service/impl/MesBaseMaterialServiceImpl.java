package com.donger.mes.system.base.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.donger.mes.system.base.entity.MesBaseMaterial;
import com.donger.mes.system.base.mapper.MesBaseMaterialMapper;
import com.donger.mes.system.base.service.MesBaseMaterialService;
import com.donger.common.core.utils.BizException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MesBaseMaterialServiceImpl extends ServiceImpl<MesBaseMaterialMapper,MesBaseMaterial> implements MesBaseMaterialService {
    @Override
    public MesBaseMaterial infoByCode(String matCode) {
        return Optional.ofNullable(this.baseMapper.selectOne(Wrappers.<MesBaseMaterial>lambdaQuery()
                .eq(MesBaseMaterial::getMatCode, matCode)
        )).orElseThrow(() -> new BizException("物料信息为空"));
    }
}
