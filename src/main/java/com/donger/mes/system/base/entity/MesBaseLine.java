package com.donger.mes.system.base.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.donger.mes.utils.entity.BaseEntity;
import com.gitee.sunchenbin.mybatis.actable.annotation.ColumnComment;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import lombok.Data;

/**
 * 产线信息
 */
@Data
@TableComment("产线信息表")
@TableName
public class MesBaseLine extends BaseEntity {


    @TableField
    @ColumnComment("产线编号")
    private String lineCode;

    @TableField
    @ColumnComment("产线名称")
    private String lineName;

    @TableField
    @ColumnComment("物料编号")
    private String matCode;

    @TableField
    @ColumnComment("物料名称")
    private String matName;


    @TableField
    @ColumnComment("产能")
    private Long capacity;


    @TableField
    @ColumnComment("产线状态 0 可用 1 不可用 2 暂停")
    private Integer status;

    


}
