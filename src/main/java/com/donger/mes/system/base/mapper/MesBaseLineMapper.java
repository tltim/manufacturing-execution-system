package com.donger.mes.system.base.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.donger.mes.system.base.entity.MesBaseLine;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MesBaseLineMapper extends BaseMapper<MesBaseLine> {
}
