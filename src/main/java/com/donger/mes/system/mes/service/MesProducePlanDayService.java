package com.donger.mes.system.mes.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.donger.mes.system.base.entity.MesBaseBom;
import com.donger.mes.system.mes.dto.MatCheckVO;
import com.donger.mes.system.mes.dto.PlanDayGanttVO;
import com.donger.mes.system.mes.entity.MesProducePlanDay;
import com.donger.mes.system.mes.dto.PlanDayFormDTO;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author pmc
 * 产品设置service
 */

public interface MesProducePlanDayService extends IService<MesProducePlanDay> {



    void release(String planCode);

    void savePlanDay(PlanDayFormDTO planDayFormDTO);

    void updataPlanDay(PlanDayFormDTO planDayFormDTO);

    void stop(String planCode);

    void addSuccsessMat(String planCode, Long num);

    List<MatCheckVO> check(String planCode);


    List<MatCheckVO> check(String matCode,Long num,List<MesBaseBom> bomList);


    List<PlanDayGanttVO> linesPlan(LocalDateTime startTime, LocalDateTime endTime);
}
