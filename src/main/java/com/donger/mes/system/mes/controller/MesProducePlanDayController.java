package com.donger.mes.system.mes.controller;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.donger.mes.system.mes.dto.MatCheckVO;
import com.donger.mes.system.mes.dto.PlanDayGanttVO;
import com.donger.mes.system.mes.dto.QueryGanttDTO;
import com.donger.mes.system.mes.entity.MesProducePlanDay;
import com.donger.mes.system.mes.dto.PlanDayFormDTO;
import com.donger.mes.system.mes.service.MaterialRequisitionService;
import com.donger.mes.system.mes.service.MesProducePlanDayService;
import com.donger.common.core.utils.Res;
import com.donger.common.core.utils.Result;
import com.donger.common.core.utils.query.enums.CommonQuery;
import com.donger.common.core.utils.query.utils.QueryGenerator;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author pmc
 * 生产日计划
 * 增删改查 基础功能
 * 发布计划
 * 暂停计划
 * 停止计划
 *
 */
@RestController
@RequestMapping("/mes/planDay")
@AllArgsConstructor
public class MesProducePlanDayController {

    private final MesProducePlanDayService mesProducePlanDayService;
    private final MaterialRequisitionService materialRequisitionService;


    /**
     * 分页查询工单数据
     * @return
     */
    @GetMapping("/page")
    public Result page(Page<MesProducePlanDay> page, MesProducePlanDay planQuery, CommonQuery commonQuery){
        QueryWrapper<MesProducePlanDay> queryWrapper = QueryGenerator.<MesProducePlanDay>initQueryWrapper(planQuery, commonQuery);
        queryWrapper.lambda().orderByAsc(MesProducePlanDay::getStatusCode);
        mesProducePlanDayService.page(page,queryWrapper);
        return Res.ok(page);
    }


    /**
     * 发布日工单计划
     * @param planCode
     * @return
     */
    @GetMapping("/release")
    public Result release(String planCode){

        /**
         * 校验日工单是否正确
         * 查询对应的产线信息
         * 根据产线信息查询对应的工作中心
         * 生成对应的工作中心数据
         */
        mesProducePlanDayService.release(planCode);
        return Res.ok();
    }


    /**
     * 新增日计划
     * @param planDayFormDTO
     * @return
     */
    @PostMapping("/save")
    public Result save(@RequestBody PlanDayFormDTO planDayFormDTO){
        mesProducePlanDayService.savePlanDay(planDayFormDTO);
        return Res.ok();
    }


    /**
     * 更新日计划
     * @param planDayFormDTO
     * @return
     */
    @PostMapping("/update")
    public Result update(@RequestBody PlanDayFormDTO planDayFormDTO){
        mesProducePlanDayService.updataPlanDay(planDayFormDTO);
        return Res.ok();
    }


    /**
     * 获取日工单详情
     * @param id
     * @return
     */
    @GetMapping("/info/{id}")
    public Result info(@PathVariable Long id) {
        MesProducePlanDay mesProducePlanDay = mesProducePlanDayService.getById(id);
        PlanDayFormDTO planDayFormDTO = new PlanDayFormDTO();
        BeanUtil.copyProperties(mesProducePlanDay,planDayFormDTO);
        return Res.ok(planDayFormDTO);
    }


    /**
     * 订单详情
     * @param id
     * @return
     */
    @GetMapping("/detail")
    public Result detail(Long id){
        MesProducePlanDay mesProducePlanDay = mesProducePlanDayService.getById(id);
        return Res.ok(mesProducePlanDay);
    }


    /**
     * 结束工单
     * @param planCode
     * @return
     */
    @GetMapping("/stop")
    public Result stop(String planCode){

        mesProducePlanDayService.stop(planCode);

        return Res.ok();
    }


    /**
     * 生成需求计划
     * @param planCode
     * @return
     */
    @GetMapping("/generateMat")
    public Result generateMat(String planCode){

        materialRequisitionService.generateMat(planCode,"");

        return Res.ok();
    }


    /**
     * 工单齐套检查
     * @param planCode
     * @return
     */
    @GetMapping("/check")
    public Result check(String planCode){

        List<MatCheckVO> check = mesProducePlanDayService.check(planCode);

        return Res.ok(check);
    }


    /**
     * 按照产线进行分组的计划
     * @return
     */
    @PostMapping("/linesPlan")
    public Result linesPlan(@RequestBody QueryGanttDTO dto){
        List<PlanDayGanttVO> planDayGanttVOList = mesProducePlanDayService.linesPlan(dto.getStartTime(), dto.getEndTime());
        return Res.ok(planDayGanttVOList);
    }







}
