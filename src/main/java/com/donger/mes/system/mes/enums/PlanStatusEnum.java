package com.donger.mes.system.mes.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 *  0:等待中；10：进行中；20：暂停；40 已关闭；100 已完成; 50 强制关闭
 */
@Getter
@AllArgsConstructor
public enum PlanStatusEnum {

    WAITING(0L,"等待中"),
    GOING(10L,"进行中"),
    PAUSED(20L,"暂停"),
    CLOSE(40L,"关闭"),
    FINISH(100L,"已完成"),
    FORCECLOSE(50L,"强制关闭");



/**
     * 类型
     */
    private Long type;
/**
     * 描述
     */
    private String description;
}
