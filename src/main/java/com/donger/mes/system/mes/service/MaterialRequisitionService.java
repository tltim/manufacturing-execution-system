package com.donger.mes.system.mes.service;


import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.donger.mes.system.mes.entity.MaterialRequisition;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface MaterialRequisitionService extends IService<MaterialRequisition> {

    /**
     * 根据工单生成对应的领料单
     * @param planCode
     */
    void generateMat(String planCode,String type);
}
