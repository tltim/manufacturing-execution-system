package com.donger.mes.system.mes.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.donger.mes.utils.entity.BaseEntity;
import com.gitee.sunchenbin.mybatis.actable.annotation.ColumnComment;
import com.gitee.sunchenbin.mybatis.actable.annotation.DefaultValue;
import com.gitee.sunchenbin.mybatis.actable.annotation.IsNotNull;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

/**
 * 投料记录
 */
@Data
@TableComment("投料记录")
@TableName
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DevoteMaterailLog extends BaseEntity {

    @TableField
    @ColumnComment("物料编码")
    @NotEmpty(message = "物料编码不能为空")
    private String matCode;

    @TableField
    @ColumnComment("物料名称")
    @NotEmpty(message = "物料名称不能为空")
    private String matName;

    @TableField
    @ColumnComment("工单编号")
    @NotEmpty(message = "工单编号不能为空")
    private String planCode;

    @TableField
    @ColumnComment("工作中心编码")
    @NotEmpty(message = "工作中心编码不能为空")
    private String workCenterCode;

    @TableField
    @ColumnComment("物料条码")
    @NotEmpty(message = "物料条码不能为空")
    private String matBarcode;

    @TableField
    @ColumnComment("投料数量")
    @IsNotNull
    @DefaultValue("0.0")
    @NotEmpty(message = "物料编码不能为空")
    private Float num;

    @TableField
    @ColumnComment("生产产品")
    private String productCode;


    @TableField
    @ColumnComment("生产产品名称")
    private String productName;

    @TableField
    @ColumnComment("供应商编码")
    private String supplierCode;


    @TableField
    @ColumnComment("供应商名称")
    private String supplierName;

    @TableField
    @ColumnComment("物料批次")
    private String matBatch;
}
