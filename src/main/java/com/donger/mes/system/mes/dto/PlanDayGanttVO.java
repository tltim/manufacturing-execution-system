package com.donger.mes.system.mes.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.gitee.sunchenbin.mybatis.actable.annotation.ColumnComment;
import com.gitee.sunchenbin.mybatis.actable.annotation.DefaultValue;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class PlanDayGanttVO {
    /**
     * 排产编号
     */

    private String planCode;

    /**
     * 计划开始时间
     */

    private LocalDateTime planStartDate;

    /**
     * 计划结束时间
     */

    private LocalDateTime planEndDate;

    /**
     * 产品编号
     */

    private String productCode;

    /**
     * 计划数量
     */

    private Long productCount;

    /**
     * 产品名称
     */

    private String productName;

    /**
     * 产线编码
     */

    private String linesCode;

    /**
     * 产线名称
     */

    private String linesName;

    /**
     * 完成数量
     */

    private Long successCount;

    /**
     * 状态 *已导入；已设置，进行中，已完成、已关闭
     */

    private String status;


    /**
     * 0:等待中；10：进行中；20：暂停；40 已关闭；100 已完成; 50 强制关闭
     */
    private Long statusCode;


    // 是否只读，，， 产线只读，不能修改
    private String  readonly;


    // 父级id  也就是产线id
    private String parent;

    private String isParent;
}
