package com.donger.mes.system.mes.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.donger.mes.system.mes.entity.WorkCenterMat;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface WorkCenterMatMapper extends BaseMapper<WorkCenterMat> {
}
