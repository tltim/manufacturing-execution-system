package com.donger.mes.system.mes.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.donger.mes.system.mes.entity.ProduceDistWorkerLog;
import com.donger.mes.system.mes.mapper.ProduceDistWorkerLogMapper;
import com.donger.mes.system.mes.service.ProduceDistWorkerLogService;
import org.springframework.stereotype.Service;

@Service
public class ProduceDistWorkerLogServiceImpl extends ServiceImpl<ProduceDistWorkerLogMapper, ProduceDistWorkerLog> implements ProduceDistWorkerLogService {
}
