package com.donger.mes.system.mes.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.donger.mes.system.mes.dto.DevoteMaterail;
import com.donger.mes.system.mes.dto.WorkDTO;
import com.donger.mes.system.mes.dto.WorkPlatformDTO;
import com.donger.mes.system.mes.entity.WorkCenterPlan;

/**
 * @author aeizzz
 */
public interface WorkCenterPlanService extends IService<WorkCenterPlan> {
    void release(String workCenterPlanCode);

    WorkCenterPlan getByWorkCenterPlanCode(String WorkCenterPlanCode);

    WorkPlatformDTO infoWorkPlatform(String workCenterPlanCode);

    void work(WorkDTO workDTO);

    void devoteMaterail(DevoteMaterail devoteMaterail);

    void suspend(String workCenterPlanCode);

    void close(String workCenterPlanCode);
}
