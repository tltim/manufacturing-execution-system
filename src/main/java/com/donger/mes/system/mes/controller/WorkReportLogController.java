package com.donger.mes.system.mes.controller;


import com.donger.mes.system.mes.entity.WorkReportLog;
import com.donger.mes.system.mes.service.WorkReportLogService;
import com.donger.mes.utils.entity.BaseController;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 报工日志
 */
@RestController
@RequestMapping("/mes/workreport")
@AllArgsConstructor
public class WorkReportLogController extends BaseController<WorkReportLogService, WorkReportLog> {
}
