package com.donger.mes.system.mes.service;

import cn.hutool.json.JSONObject;
import com.donger.mes.system.mes.entity.ChangepointLog;

public interface ChangepointLogService {


    void save(ChangepointLog changepointLog, JSONObject jsonObject);
}
