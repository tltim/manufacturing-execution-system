package com.donger.mes.system.mes.event.listener;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.donger.mes.system.base.entity.MesBaseWorkCenter;
import com.donger.mes.system.base.mapper.MesBaseWorkCenterMapper;
import com.donger.mes.system.base.service.MesBaseWorkCenterService;
import com.donger.mes.system.mes.entity.WorkCenterPlan;
import com.donger.mes.system.mes.event.WorkCenterWorkEvent;
import com.donger.mes.system.mes.service.MesProducePlanDayService;
import com.donger.common.core.utils.BizException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.Optional;


/**
 * 工作中心事件接受处理
 */
@Component
@Slf4j
@AllArgsConstructor
public class WorkCenterListener {
    private final MesBaseWorkCenterMapper mesBaseWorkCenterMapper;
    private final MesProducePlanDayService mesProducePlanDayService;


    /**
     *
     * @param workCenterWorkEvent
     */
    @EventListener
    public void onApplicationEvent(WorkCenterWorkEvent workCenterWorkEvent) {

        WorkCenterPlan workCenterPlan = workCenterWorkEvent.getWorkCenterPlan();

        MesBaseWorkCenter mesBaseWorkCenter = Optional.ofNullable(mesBaseWorkCenterMapper.selectOne(Wrappers.<MesBaseWorkCenter>lambdaQuery()
                .eq(MesBaseWorkCenter::getCode, workCenterPlan.getWorkCenterCode())
        )).orElseThrow(() -> new BizException("工作中心为空"));
        // 更新计划报工数量情况
        if("finish".equals(mesBaseWorkCenter.getType()) && "output".equals(mesBaseWorkCenter.getMode())){
            mesProducePlanDayService.addSuccsessMat(workCenterPlan.getPlanCode(),workCenterWorkEvent.getNum());
        }



    }



}
