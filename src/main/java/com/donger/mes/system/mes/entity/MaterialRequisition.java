package com.donger.mes.system.mes.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.donger.mes.utils.entity.BaseEntity;
import com.gitee.sunchenbin.mybatis.actable.annotation.ColumnComment;
import com.gitee.sunchenbin.mybatis.actable.annotation.IsNotNull;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 领料单
 */
@Data
@TableComment("领料单")
@TableName
public class MaterialRequisition extends BaseEntity {

    @TableField
    @ColumnComment("物料编码")
    @IsNotNull
    private String matCode;
    @TableField
    @ColumnComment("物料名称")
    private String matName;

    @TableField
    @ColumnComment("需求数量")
    private Float num;

    @TableField
    @ColumnComment("完成配送数量")
    private Float successNum;

    @TableField
    @ColumnComment("产品编码")
    private String productCode;
    @TableField
    @ColumnComment("产品名称")
    private String productName;

    @TableField
    @ColumnComment("产品数量")
    private Long productNum;
    @TableField
    @ColumnComment("工单编码")
    private String planCode;
    @TableField
    @ColumnComment("工作中心")
    private String workCenterCode;
    @TableField
    @ColumnComment("配送时间")
    private LocalDateTime deliveryTime;
    @TableField
    @ColumnComment("需求班次")
    private String banci;

    @TableField
    @ColumnComment("配送库位")
    private String siteCode;


}
