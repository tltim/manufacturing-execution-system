package com.donger.mes.system.mes.controller;

import com.donger.mes.system.mes.entity.ProduceDistWorkerLog;
import com.donger.mes.system.mes.service.ProduceDistWorkerLogService;
import com.donger.mes.utils.entity.BaseController;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 生产派工日志
 */
@RestController
@RequestMapping("/mes/distworker")
@AllArgsConstructor
public class ProduceDistWorkerLogController extends BaseController<ProduceDistWorkerLogService, ProduceDistWorkerLog> {
}
