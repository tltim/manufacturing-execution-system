package com.donger.mes.system.mes.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.donger.mes.system.mes.entity.WorkCenterProduct;
import com.donger.mes.system.mes.mapper.WorkCenterProductMapper;
import com.donger.mes.system.mes.service.WorkCenterProductService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;


/**
 * @author aeizzz
 */
@Service
@AllArgsConstructor
public class WorkCenterProductServiceImpl implements WorkCenterProductService {
    private final WorkCenterProductMapper workCenterProductMapper;
    /**
     * 新增
     * @param workCenterProduct
     */
    @Override
    public void save(WorkCenterProduct workCenterProduct) {
        workCenterProductMapper.insert(workCenterProduct);

    }

    @Override
    public IPage<WorkCenterProduct> page(IPage<WorkCenterProduct> page, QueryWrapper<WorkCenterProduct> queryWrapper) {
        return workCenterProductMapper.selectPage(page,queryWrapper);
    }
}
