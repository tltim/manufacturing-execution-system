package com.donger.mes.system.mes.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.donger.mes.system.mes.entity.ChangepointLog;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ChangepointLogMapper extends BaseMapper<ChangepointLog> {
}
