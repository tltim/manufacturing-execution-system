package com.donger.mes.system.mes.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.donger.mes.system.mes.entity.WorkCenterMat;
import com.donger.mes.system.mes.entity.WorkCenterProduct;
import com.donger.mes.system.mes.service.WorkCenterProductService;
import com.donger.common.core.utils.Res;
import com.donger.common.core.utils.Result;
import com.donger.common.core.utils.query.enums.CommonQuery;
import com.donger.common.core.utils.query.utils.QueryGenerator;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author aeizzz
 */
@RestController
@RequestMapping("/mes/product")
@AllArgsConstructor
public class WorkCenterProductController {
    private final WorkCenterProductService workCenterProductService;


    /**
     * 分页查询工单数据
     *
     * @return
     */
    @GetMapping("/page")
    public Result page(Page<WorkCenterProduct> page, WorkCenterProduct workCenterProduct, CommonQuery commonQuery) {
        QueryWrapper<WorkCenterProduct> queryWrapper = QueryGenerator.initQueryWrapper(workCenterProduct, commonQuery);
        queryWrapper.lambda().orderByDesc(WorkCenterProduct::getCreatedTime);
        workCenterProductService.page(page, queryWrapper);
        return Res.ok(page);
    }
}
