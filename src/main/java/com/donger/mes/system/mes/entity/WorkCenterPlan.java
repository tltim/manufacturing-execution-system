package com.donger.mes.system.mes.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.Version;
import com.gitee.sunchenbin.mybatis.actable.annotation.ColumnComment;
import com.gitee.sunchenbin.mybatis.actable.annotation.DefaultValue;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 工作中心计划
 * @author pmc
 */
@TableName
@Data
@TableComment("工作中心生产计划")
public class WorkCenterPlan {

    @TableId
    private Long id;


    @ColumnComment("工作中心计划编码")
    @TableField
    private String workCenterPlanCode;

    /**
     * 工作中心名称
     */
    @ColumnComment("工作中心名称")
    @TableField
    private String name;
    /**
     * 编码
     */
    @ColumnComment("编码")
    @TableField
    private String workCenterCode;
    /**
     * 设备编码
     */
    @ColumnComment("设备编码")
    @TableField
    private String deviceCode;

    /**
     * 设备名称
     */
    @ColumnComment("设备名称")
    @TableField
    private String deviceName;

    /**
     * 工位
     */
    @ColumnComment("工位")
    @TableField
    private String station;


    @ColumnComment("工位名称")
    @TableField
    private String stationName;

    /**
     * 产品编码
     */
    @ColumnComment("产品编码")
    @TableField
    private String productCode;


    /**
     * 产品名称
     */
    @ColumnComment("产品名称")
    @TableField
    private String productName;

    /**
     * 计划数量
     */
    @ColumnComment("计划数量")
    @TableField
    private Long productCount;

    /**
     * 完成数量
     */
    @ColumnComment("完成数量")
    @TableField
    @DefaultValue("0")
    private Long successCount;

    /**
     * 状态 *已导入；已设置，进行中，已完成、已关闭
     */
    @ColumnComment("状态: 等待中，进行中，暂停, 关闭, 已完成, 强制关闭")
    @TableField
    private String status;



    /**
     * 0:等待中；10：进行中；20：暂停；40 已关闭；100 已完成; 50 强制关闭
     */
    @ColumnComment("0:等待中；10：进行中；20：暂停；40 已关闭；100 已完成; 50 强制关闭")
    @TableField
    private Long statusCode;


    /**
     * 排产编号
     */
    @ColumnComment("排产编号")
    @TableField
    private String planCode;


    @ColumnComment("首次执行班次")
    @TableField
    private String banci;

    /**
     * 计划开始时间
     */
    @ColumnComment("计划开始时间")
    @TableField
    private LocalDateTime planStartDate;

    /**
     * 计划结束时间
     */
    @ColumnComment("计划结束时间")
    @TableField
    private LocalDateTime planEndDate;

    /**
     * 实际开始时间
     */
    @ColumnComment("实际开始时间")
    @TableField
    private LocalDateTime realityStartDate;

    /**
     * 实际结束时间
     */
    @ColumnComment("实际结束时间")
    @TableField
    private LocalDateTime realityEndDate;


    /**
     * 预计完成时间
     */
    @ColumnComment("预计完成时间")
    @TableField
    private LocalDateTime estimatedCompletionTime;

    /**
     * 每天生产速度(小时)
     */
    @ColumnComment("每天生产速度(小时)")
    @TableField
    private Long productSpeed;



    @ColumnComment("工作模式")
    @TableField
    private String mode;


    @ColumnComment("版本号")
    @TableField
    @Version
    @DefaultValue("0")
    private Long version;



}
