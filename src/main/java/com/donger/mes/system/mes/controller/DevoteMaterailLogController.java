package com.donger.mes.system.mes.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.donger.common.core.utils.Res;
import com.donger.common.core.utils.Result;
import com.donger.common.core.utils.query.enums.CommonQuery;
import com.donger.common.core.utils.query.utils.QueryGenerator;
import com.donger.mes.system.mes.entity.DevoteMaterailLog;
import com.donger.mes.system.mes.service.DevoteMaterailLogService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/devote/mat")
@AllArgsConstructor
public class DevoteMaterailLogController {

    private final DevoteMaterailLogService devoteMaterailLogService;


    /**
     * 分页查询工单数据
     *
     * @return
     */
    @GetMapping("/page")
    public Result page(Page<DevoteMaterailLog> page, DevoteMaterailLog devoteMaterailLog, CommonQuery commonQuery) {
        QueryWrapper<DevoteMaterailLog> queryWrapper = QueryGenerator.initQueryWrapper(devoteMaterailLog, commonQuery);
        queryWrapper.lambda().orderByAsc(DevoteMaterailLog::getCreatedTime);
        devoteMaterailLogService.page(page, queryWrapper);
        return Res.ok(page);
    }
}
