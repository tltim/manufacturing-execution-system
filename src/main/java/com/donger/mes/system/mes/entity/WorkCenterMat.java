package com.donger.mes.system.mes.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.donger.mes.utils.entity.BaseEntity;
import com.gitee.sunchenbin.mybatis.actable.annotation.ColumnComment;
import com.gitee.sunchenbin.mybatis.actable.annotation.DefaultValue;
import com.gitee.sunchenbin.mybatis.actable.annotation.IsNotNull;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import lombok.Data;

/**
 * 记录该工作重心上对应工单内的物料
 */
@Data
@TableName
@TableComment("工作中心物料仓库表")
public class WorkCenterMat extends BaseEntity {

    @TableField
    @ColumnComment("工作中心编码")
    @IsNotNull
    private String workCenterCode;
    @TableField
    @ColumnComment("工作中心名称")
    @IsNotNull
    private String workCenterName;
    @TableField
    @ColumnComment("物料编码")
    @IsNotNull
    private String matCode;

    @TableField
    @ColumnComment("物料名称")
    @IsNotNull
    private String matName;


    @TableField
    @ColumnComment("物料条码")
    @IsNotNull
    private String matBarcode;


    @TableField
    @ColumnComment("入库数量")
    @IsNotNull
    @DefaultValue("0.0")
    private Float num;


    @TableField
    @ColumnComment("工单编号")
    @IsNotNull
    private String planCode;

    @TableField
    @ColumnComment("生产产品")
    private String productCode;


    @TableField
    @ColumnComment("生产产品名称")
    private String productName;


    @TableField
    @ColumnComment("供应商编码")
    private String supplierCode;


    @TableField
    @ColumnComment("供应商名称")
    private String supplierName;


    @TableField
    @ColumnComment("物料批次")
    private String matBatch;

    /**
     * 是否冻结 0未冻结 1已冻结
     */
    @TableField
    @ColumnComment("是否冻结 0 未冻结 1 已冻结")
    @DefaultValue("0")
    private String isFrozen;

    /**
     * 当前数量
     */
    @TableField
    @ColumnComment("当前数量")
    @IsNotNull
    @DefaultValue("0.0")
    private Float currentNumber;

    /**
     * 备注信息
     */
    @TableField
    @ColumnComment("备注")
    private String remarks;



}
