package com.donger.mes.system.mes.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class QueryGanttDTO {
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private String deviceCode;
    private String deviceName;
    private String productName;
}
