package com.donger.mes.system.mes.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.donger.mes.system.mes.entity.MaterialRequisition;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MaterialRequisitionMapper extends BaseMapper<MaterialRequisition> {
}
