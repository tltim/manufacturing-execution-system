package com.donger.mes.system.mes.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.donger.common.core.utils.BizException;
import com.donger.mes.system.base.entity.MesBaseBom;
import com.donger.mes.system.base.service.MesBaseBomService;
import com.donger.mes.system.mes.entity.MaterialRequisition;
import com.donger.mes.system.mes.entity.MesProducePlanDay;
import com.donger.mes.system.mes.mapper.MaterialRequisitionMapper;
import com.donger.mes.system.mes.service.MaterialRequisitionService;
import com.donger.mes.system.mes.service.MesProducePlanDayService;
import com.donger.mes.utils.mes.MesUtils;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


/**
 * 领料单生成
 */
@Service
@AllArgsConstructor
public class MaterialRequisitionServiceImpl extends ServiceImpl<MaterialRequisitionMapper, MaterialRequisition> implements MaterialRequisitionService {
    private final MesProducePlanDayService mesProducePlanDayService;
    private final MesBaseBomService mesBaseBomService;


    /**
     * 生成领料单
     *
     * @param planCode
     */
    @Override
    public void generateMat(String planCode, String type) {
        // 查询工单
        MesProducePlanDay planDay = mesProducePlanDayService.getOne(Wrappers.<MesProducePlanDay>lambdaQuery()
                .eq(MesProducePlanDay::getPlanCode, planCode)
        );
        // 判断是否能生成
//        if(!planDay.getStatusCode().equals(PlanStatusEnum.GOING.getType())){
//            throw new BizException("工单状态不是进行中，不能生成领料单");
//        }

        // 根据生成状态，如果是重新初始化则，删除原有所有领料单内容

        baseMapper.delete(Wrappers.<MaterialRequisition>lambdaQuery()
                .eq(MaterialRequisition::getPlanCode, planCode));

        // 查询对应的bom 内容，进行领料单的生成

        List<MesBaseBom> bomList = mesBaseBomService.list(planDay.getProductCode());
        if (CollUtil.isEmpty(bomList)) {
            throw new BizException("bom为空不能进行生成领料单");
        }
        // 生成对应的领料单
        List<MaterialRequisition> materialRequisitions = bomList.stream().map(item -> {
            MaterialRequisition materialRequisition = new MaterialRequisition();
            materialRequisition.setMatName(item.getMatName());
            materialRequisition.setMatCode(item.getMatCode());
            materialRequisition.setPlanCode(planDay.getPlanCode());
            materialRequisition.setProductCode(planDay.getProductCode());
            materialRequisition.setProductName(planDay.getProductName());
            // TODO 提前三十分钟配送
            materialRequisition.setDeliveryTime(planDay.getPlanStartDate().minusMinutes(30));
            materialRequisition.setBanci(MesUtils.calcBanciByDate(materialRequisition.getDeliveryTime()));
            // TODO 需求直接计算 ， 如果需要多领则需要单独配置
            materialRequisition.setNum(planDay.getProductCount() * item.getCount());
            materialRequisition.setProductNum(planDay.getProductCount());
            materialRequisition.setSuccessNum(0f);
            return materialRequisition;
        }).collect(Collectors.toList());
        this.saveBatch(materialRequisitions);
    }


}
