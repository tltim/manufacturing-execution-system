package com.donger.mes.system.mes.dto;

import lombok.Data;


/**
 * 投料数据
 */
@Data
public class DevoteMaterail {
    private String matBarcode;
    private String planCode;
    private String workCenterCode;
    private float number;
}
