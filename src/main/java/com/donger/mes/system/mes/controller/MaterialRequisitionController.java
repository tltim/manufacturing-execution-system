package com.donger.mes.system.mes.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.donger.common.core.utils.Res;
import com.donger.common.core.utils.Result;
import com.donger.common.core.utils.query.enums.CommonQuery;
import com.donger.common.core.utils.query.utils.QueryGenerator;
import com.donger.mes.system.mes.entity.DevoteMaterailLog;
import com.donger.mes.system.mes.entity.MaterialRequisition;
import com.donger.mes.system.mes.service.DevoteMaterailLogService;
import com.donger.mes.system.mes.service.MaterialRequisitionService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 领料单查询
 */
@RestController
@RequestMapping("/mes/matreq")
@AllArgsConstructor
public class MaterialRequisitionController {

    private final MaterialRequisitionService materialRequisitionService;


    /**
     * 分页查询工单数据
     *
     * @return
     */
    @GetMapping("/page")
    public Result page(Page<MaterialRequisition> page, MaterialRequisition materialRequisition, CommonQuery commonQuery) {
        QueryWrapper<MaterialRequisition> queryWrapper = QueryGenerator.initQueryWrapper(materialRequisition, commonQuery);
        queryWrapper.lambda().orderByAsc(MaterialRequisition::getCreatedTime);
        materialRequisitionService.page(page, queryWrapper);
        return Res.ok(page);
    }
}
