package com.donger.mes.system.mes.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.donger.mes.utils.entity.BaseEntity;
import com.gitee.sunchenbin.mybatis.actable.annotation.ColumnComment;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import lombok.Data;

/**
 * 工作中心生产
 * @author aeizzz
 */
@Data
@TableName
@TableComment("工作中心生产数据")
public class WorkCenterProduct extends BaseEntity {

    @ColumnComment("工作中心计划编码")
    @TableField
    private String workCenterPlanCode;


    /**
     * 产品编码
     */
    @ColumnComment("包装物编码")
    @TableField
    private String packageCode;

    @ColumnComment("产品编码")
    @TableField
    private String productCode;


    @ColumnComment("产品名称")
    @TableField
    private String productName;
    /**
     * 数量
     */
    @ColumnComment("数量")
    @TableField
    private Long num;

    /**
     * 类型 一物一码  成品箱码 半成品箱码
     */
    @ColumnComment("类型")
    @TableField
    private String type;


    @ColumnComment("工单编码")
    @TableField
    private String planCode;



    @ColumnComment("操作员工")
    @TableField
    private String employeeCode;


    @ColumnComment("班次")
    @TableField
    private String banci;


    @ColumnComment("批次")
    @TableField
    private String batch;



    @ColumnComment("二级包装物")
    @TableField
    private String boxCode;



    /**
     * 返工返修产品标记
     */
    @ColumnComment("返工返修标记")
    @TableField
    private String isolate = "0";

    /**
     * 返工返修数量
     */
    @ColumnComment("返工返修数量")
    @TableField
    private Long unqualifiedNum;


    @ColumnComment("状态")
    @TableField
    private String status;






}
