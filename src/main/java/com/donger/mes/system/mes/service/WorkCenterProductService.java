package com.donger.mes.system.mes.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.donger.mes.system.mes.entity.WorkCenterProduct;

public interface WorkCenterProductService {
    void save(WorkCenterProduct workCenterProduct);

    IPage<WorkCenterProduct> page(IPage<WorkCenterProduct> page, QueryWrapper<WorkCenterProduct> queryWrapper);
}
