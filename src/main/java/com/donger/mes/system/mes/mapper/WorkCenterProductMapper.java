package com.donger.mes.system.mes.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.donger.mes.system.mes.entity.WorkCenterProduct;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author aeizzz
 */
@Mapper
public interface WorkCenterProductMapper extends BaseMapper<WorkCenterProduct> {
}
