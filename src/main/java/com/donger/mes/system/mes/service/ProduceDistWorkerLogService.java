package com.donger.mes.system.mes.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.donger.mes.system.mes.entity.ProduceDistWorkerLog;

public interface ProduceDistWorkerLogService extends IService<ProduceDistWorkerLog> {
}
