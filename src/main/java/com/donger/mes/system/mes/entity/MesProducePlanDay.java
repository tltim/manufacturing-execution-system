package com.donger.mes.system.mes.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.donger.mes.utils.entity.BaseEntity;
import com.gitee.sunchenbin.mybatis.actable.annotation.ColumnComment;
import com.gitee.sunchenbin.mybatis.actable.annotation.DefaultValue;
import com.gitee.sunchenbin.mybatis.actable.annotation.IsNotNull;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * MES-生产-生产日计划
 *
 * @author somliy
 * @since 2020-06-12 17:30:24
 */
@Data
@TableName
@TableComment("日生产计划")
public class MesProducePlanDay extends BaseEntity implements Serializable {

    private static final long serialVersionUID = -1;

    /**
     * 合同订单编号
     */
    @ColumnComment("合同订单编号")
    @TableField
    @IsNotNull
    private String orderId;

    /**
     * 计划开始时间
     */
    @ColumnComment("计划开始时间")
    @TableField
    private LocalDateTime planStartDate;

    /**
     * 计划结束时间
     */
    @ColumnComment("计划结束时间")
    @TableField
    private LocalDateTime planEndDate;

    /**
     * 实际开始时间
     */
    @ColumnComment("实际开始时间")
    @TableField
    private LocalDateTime realityStartDate;

    /**
     * 实际结束时间
     */
    @ColumnComment("实际结束时间")
    @TableField
    private LocalDateTime realityEndDate;


    /**
     * 预计完成时间
     */
    @ColumnComment("预计完成时间")
    @TableField
    private LocalDateTime estimatedCompletionTime;


    /**
     * 排产编号
     */
    @ColumnComment("排产编号")
    @TableField
    private String planCode;


    /**
     * 产品编号
     */
    @ColumnComment("产品编号")
    @TableField
    private String productCode;

    /**
     * 计划数量
     */
    @ColumnComment("计划数量")
    @TableField
    private Long productCount;

    /**
     * 产品名称
     */
    @ColumnComment("产品名称")
    @TableField
    private String productName;


    /**
     * 产线编码
     */
    @ColumnComment("产线编码")
    @TableField
    private String linesCode;

    /**
     * 产线名称
     */
    @ColumnComment("产线名称")
    @TableField
    private String linesName;

    /**
     * 完成数量
     */
    @ColumnComment("完成数量")
    @TableField
    @DefaultValue("0")
    private Long successCount;


    /**
     * 状态 *已导入；已设置，进行中，已完成、已关闭
     */
    @ColumnComment("状态: 等待中，进行中，暂停, 关闭, 已完成, 强制关闭")
    @TableField
    private String status;


    /**
     * 0:等待中；10：进行中；20：暂停；40 已关闭；100 已完成; 50 强制关闭
     */
    @ColumnComment("0:等待中；10：进行中；20：暂停；40 已关闭；100 已完成; 50 强制关闭")
    @TableField
    private Long statusCode;


    /**
     * 每天生产速度(小时)
     */
    @ColumnComment("每天生产速度(小时)")
    @TableField
    private Long productSpeed;


    /**
     * 首次执行班次
     */
    @ColumnComment("首次执行班次")
    @TableField
    private String banci;

    /**
     * 周计划编号
     */
    @ColumnComment("周计划编号")
    @TableField
    private String planCodeWeek;



    @ColumnComment("领料单是否生成 1生成 0 未生成")
    @TableField
    @DefaultValue("0")
    private String requestion;
}