package com.donger.mes.system.mes.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.donger.mes.system.mes.entity.WorkCenterMat;
import com.donger.mes.system.mes.entity.WorkCenterPlan;
import com.donger.mes.system.mes.service.WorkCenterMatService;
import com.donger.mes.system.mes.service.WorkCenterPlanService;
import com.donger.common.core.utils.Res;
import com.donger.common.core.utils.Result;
import com.donger.common.core.utils.query.enums.CommonQuery;
import com.donger.common.core.utils.query.utils.QueryGenerator;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/mes/Mat")
@AllArgsConstructor
public class WorkCenterMatController {

    private final WorkCenterMatService workCenterMatService;
    private final WorkCenterPlanService workCenterPlanService;


    /**
     * 分页查询工单数据
     *
     * @return
     */
    @GetMapping("/page")
    public Result page(Page<WorkCenterMat> page, WorkCenterMat workCenterMat, CommonQuery commonQuery) {
        QueryWrapper<WorkCenterMat> queryWrapper = QueryGenerator.initQueryWrapper(workCenterMat, commonQuery);
        queryWrapper.lambda().orderByDesc(WorkCenterMat::getCreatedTime);
        workCenterMatService.page(page, queryWrapper);
        return Res.ok(page);
    }


    /**
     * 退料
     * @return
     */
    @GetMapping("/returnMat")
    public Result returnMaterial(String workCenterPlanCode){
        WorkCenterPlan workCenterPlan = workCenterPlanService.getByWorkCenterPlanCode(workCenterPlanCode);


        workCenterMatService.returnMaterial(workCenterPlan.getWorkCenterCode(),workCenterPlan.getPlanCode());



        return Res.ok();
    }








}
