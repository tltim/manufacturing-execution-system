package com.donger.mes.system.mes.dto;


import lombok.Data;

@Data
public class WorkCenterMatPlatformDTO {

    private String matCode;


    private String matName;

    private Float num;
}
