package com.donger.mes.system.mes.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.donger.mes.utils.entity.BaseEntity;
import com.gitee.sunchenbin.mybatis.actable.annotation.ColumnComment;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import lombok.Data;

/**
 * 变化点数据
 */
@Data
@TableName
@TableComment("变化点")
public class ChangepointLog extends BaseEntity {

    /**
     * 变化点编号
     */
    @ColumnComment("变化点编号")
    private String code;

    /**
     * 类型
     */
    @ColumnComment("类型")
    private String type;

    /**
     * 变化点描述
     */
    @ColumnComment("变化点描述")
    private String content;


    /**
     * 对应产品编号
     */
    @ColumnComment("对应产品编号")
    private String productCode;

    /**
     * 对应产品名称
     */
    @ColumnComment("对应产品名称")
    private String productName;


    /**
     * json数据，自行扩展
     */
    @ColumnComment("json数据")
    private String jsonData;
}
