package com.donger.mes.system.mes.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.donger.mes.system.mes.entity.WorkReportLog;
import com.donger.mes.system.mes.mapper.WorkReportLogMapper;
import com.donger.mes.system.mes.service.WorkReportLogService;
import org.springframework.stereotype.Service;

import javax.validation.Valid;

@Service
public class WorkReportLogServiceImpl extends ServiceImpl<WorkReportLogMapper, WorkReportLog> implements WorkReportLogService {

    /**
     * 生产报工日志保存
     * @param build
     */
    @Override
    public void saveWorkReport(@Valid WorkReportLog build) {

        this.save(build);
    }
}
