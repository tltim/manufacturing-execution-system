package com.donger.mes.system.mes.service.impl;

import cn.hutool.json.JSONObject;
import com.donger.mes.system.mes.entity.ChangepointLog;
import com.donger.mes.system.mes.service.ChangepointLogService;
import org.springframework.stereotype.Service;


/**
 * 变化点业务类型
 */
@Service
public class ChangepointLogServiceImpl implements ChangepointLogService {


    /**
     * 新增变化点
     * @param changepointLog 变化点数据
     * @param jsonObject 附加数据
     */
    @Override
    public void save(ChangepointLog changepointLog, JSONObject jsonObject) {

    }
}
