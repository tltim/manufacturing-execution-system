package com.donger.mes.system.mes.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.donger.mes.system.mes.entity.WorkReportLog;

public interface WorkReportLogService extends IService<WorkReportLog> {
    void saveWorkReport(WorkReportLog build);
}
