package com.donger.mes.system.mes.work;

import com.donger.mes.system.base.entity.MesBaseBom;
import com.donger.mes.system.base.entity.MesBaseMaterial;
import com.donger.mes.system.mes.dto.WorkDTO;
import com.donger.mes.system.mes.entity.WorkCenterPlan;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 合并报工，根据产品对应的客户信息进行自动装箱报工，打印出对应的箱码
 */
@Component(value = "mergeCode")
public class MergeCodeWorkHandle implements WorkHandle{
    @Override
    public WorkCenterPlan execute(WorkCenterPlan workCenterPlan, WorkDTO workDTO) {
        return null;
    }

    @Override
    public void checkData(WorkCenterPlan workCenterPlan, WorkDTO workDTO) {

    }

    @Override
    public void backflush(WorkCenterPlan workCenterPlan, List<MesBaseBom> materialList, Long produceNum) {

    }


    @Override
    public void produce(String type, WorkCenterPlan workCenterPlan, WorkDTO workDTO, MesBaseMaterial mesBaseMaterial) {

    }
}
