package com.donger.mes.system.mes.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.donger.mes.utils.entity.BaseEntity;
import com.gitee.sunchenbin.mybatis.actable.annotation.ColumnComment;
import com.gitee.sunchenbin.mybatis.actable.annotation.IsNotNull;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import lombok.Data;

/**
 * 生产派工日志
 */
@Data
@TableName
@TableComment("生产派工日志")
public class ProduceDistWorkerLog extends BaseEntity {

    /**
     * 员工编号
     */
    @TableField
    @ColumnComment("员工编码")
    @IsNotNull
    private String employeeCode;

    @TableField
    @ColumnComment("员工姓名")
    private String employeeName;

    @TableField
    @ColumnComment("工单编码")
    @IsNotNull
    private String planCode;

    @TableField
    @ColumnComment("工作中心编码")
    @IsNotNull
    private String workCenterCode;
    /**
     * 班次
     */
    @TableField
    @ColumnComment("班次")
    @IsNotNull
    private String banci;

    /**
     * 产品名称
     */
    @TableField
    @ColumnComment("产品名称")
    @IsNotNull
    private String productName;

    /**
     * 产品编号
     */
    @TableField
    @ColumnComment("产品编号")
    @IsNotNull
    private String productCode;


    /**
     * 删除标记 0未删除 1已删除
     */
    @TableField
    @ColumnComment("删除标记 0未删除 1已删除")
    private String isDel;


    /**
     * 派工标记 0未派工 1已派工
     */
    @TableField
    @ColumnComment("派工标记 0未派工 1已派工")
    private String isWork;
}
