package com.donger.mes.system.mes.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.donger.mes.system.mes.entity.DevoteMaterailLog;

public interface DevoteMaterailLogService extends IService<DevoteMaterailLog> {


    void saveDevoteLog(DevoteMaterailLog devoteMaterailLog);
}
