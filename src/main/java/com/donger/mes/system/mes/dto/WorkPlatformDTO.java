package com.donger.mes.system.mes.dto;

import com.donger.mes.system.mes.entity.WorkCenterPlan;
import lombok.Data;

import java.util.List;

@Data
public class WorkPlatformDTO{

    // 工单信息

    private Long id;
    private String workCenterCode;
    private String workCenterName;

    private String workCenterPlanCode;

    private String deviceCode;

    private String deviceName;
    private String station;
    private String stationName;

    private String productCode;

    private String productName;

    private Long productCount;

    private Long successCount;

    private String status;
    private String statusCode;

    private String planCode;

    private String banci;





    // 员工信息
    private String employee;


    // 物料剩余信息

    List<WorkCenterMatPlatformDTO> workCenterMatPlatformDTOList;

    // 后续工单信息

    List<WorkCenterPlan> workCenterPlanList;

    // 最近生产信息
}
