package com.donger.mes.system.mes.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.donger.mes.utils.entity.BaseEntity;
import com.gitee.sunchenbin.mybatis.actable.annotation.ColumnComment;
import com.gitee.sunchenbin.mybatis.actable.annotation.IsNotNull;
import com.gitee.sunchenbin.mybatis.actable.annotation.TableComment;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

/**
 * 报工记录
 */
@Data
@TableName
@TableComment("报工记录")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class WorkReportLog extends BaseEntity {

    @TableField
    @ColumnComment("工单号")
    @IsNotNull
    @NotEmpty(message = "工单号不能为空")
    private String planCode;
    @TableField
    @ColumnComment("工作中心编码")
    @IsNotNull
    @NotEmpty(message = "工作中心编码不能为空")
    private String workCenterCode;
    @TableField
    @ColumnComment("产品编码")
    @IsNotNull
    @NotEmpty(message = "产品编码不能为空")
    private String matCode;
    @TableField
    @ColumnComment("产品名称")
    @IsNotNull
    @NotEmpty(message = "产品名称不能为空")
    private String matName;
    @TableField
    @ColumnComment("数量")
    @IsNotNull
    @NotEmpty(message = "数量不能为空")
    private Long num;

    @TableField
    @ColumnComment("箱码/包装物吗")
    private String boxCode;

}
