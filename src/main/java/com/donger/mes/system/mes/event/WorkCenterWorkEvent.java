package com.donger.mes.system.mes.event;

import com.donger.mes.system.mes.entity.WorkCenterPlan;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

/**
 * 工作中心报工事件
 */


@Getter
public class WorkCenterWorkEvent extends ApplicationEvent {

    // 工作中心计划
    private WorkCenterPlan workCenterPlan;

    // 报工数量
    private Long num;

    public WorkCenterWorkEvent(WorkCenterPlan workCenterPlan) {
        super(workCenterPlan);
        this.workCenterPlan = workCenterPlan;
    }

    public WorkCenterWorkEvent(WorkCenterPlan workCenterPlan,Long num) {
        super(workCenterPlan);
        this.workCenterPlan = workCenterPlan;
        this.num = num;
    }
}
