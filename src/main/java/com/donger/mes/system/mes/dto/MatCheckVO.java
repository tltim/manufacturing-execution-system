package com.donger.mes.system.mes.dto;

import lombok.Data;

@Data
public class MatCheckVO {

    // 物料编码
    private String matCode;
    // 物料名称
    private String matName;
    // 需求量
    private Float num;
    // 当前线边
    private Float surplusNum;

    // 仓库量
    private Float wmsNum;
}
