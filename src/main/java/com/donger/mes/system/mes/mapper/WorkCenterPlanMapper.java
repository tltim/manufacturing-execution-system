package com.donger.mes.system.mes.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.donger.mes.system.mes.entity.WorkCenterPlan;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface WorkCenterPlanMapper extends BaseMapper<WorkCenterPlan> {
}
