package com.donger.mes.system.mes.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.donger.common.core.utils.Res;
import com.donger.common.core.utils.Result;
import com.donger.common.core.utils.query.enums.CommonQuery;
import com.donger.common.core.utils.query.utils.QueryGenerator;
import com.donger.mes.system.mes.dto.DevoteMaterail;
import com.donger.mes.system.mes.dto.WorkDTO;
import com.donger.mes.system.mes.dto.WorkPlatformDTO;
import com.donger.mes.system.mes.entity.WorkCenterPlan;
import com.donger.mes.system.mes.service.WorkCenterPlanService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 工作中心计划
 * 查询工作中心数据
 * 执行工作中心任务
 */
@RestController
@RequestMapping("/mes/workcenter")
@AllArgsConstructor
public class WorkCenterPlanController {

    private final WorkCenterPlanService workCenterPlanService;


    /**
     * 查询工作中心任务列表
     *
     * @param page
     * @param query
     * @param commonQuery
     * @return
     */
    @GetMapping("/page")
    public Result page(Page page, WorkCenterPlan query, CommonQuery commonQuery) {
        QueryWrapper<WorkCenterPlan> queryWrapper = QueryGenerator.initQueryWrapper(query, commonQuery);
        queryWrapper.lambda().orderByAsc(WorkCenterPlan::getStatusCode);
        workCenterPlanService.page(page, queryWrapper);
        return Res.ok(page);
    }


    /**
     * 工作中心计划启动
     *
     * @return
     */
    @GetMapping("/release")
    public Result release(String workCenterPlanCode) {
        workCenterPlanService.release(workCenterPlanCode);
        return Res.ok();
    }

    /**
     * 工作中心计划停止
     *
     * @return
     */
    @GetMapping("/close")
    public Result close(String workCenterPlanCode) {
        workCenterPlanService.close(workCenterPlanCode);
        return Res.ok();
    }


    /**
     * 暂停计划
     *
     * @return
     */
    @GetMapping("/suspend")
    public Result suspend(String workCenterPlanCode) {
        workCenterPlanService.suspend(workCenterPlanCode);
        return Res.ok();
    }


    /**
     * 报工
     *
     * @return
     */
    @PostMapping("/work")
    public Result work(@RequestBody @Valid WorkDTO workDTO) {
        if (workDTO.getNum() == null) {
            workDTO.setNum(1L);
        }
        workCenterPlanService.work(workDTO);
        return Res.ok();
    }

    /**
     * 投料
     *
     * @return
     */
    @PostMapping("/feeting")
    public Result devoteMaterail(@RequestBody DevoteMaterail devoteMaterail) {

        // 投料数据校验
        workCenterPlanService.devoteMaterail(devoteMaterail);

        return Res.ok();
    }


    /**
     * 工作台可疑品录入可疑品录入
     *
     * @return
     */
    public Result suspect() {


        return Res.ok();
    }


    /**
     * 查询工作台详情数据
     *
     * @return
     */
    @GetMapping("/workPlanform")
    public Result infoWorkPlatform(String workCenterPlanCode) {
        WorkPlatformDTO workPlatformDTO = workCenterPlanService.infoWorkPlatform(workCenterPlanCode);
        return Res.ok(workPlatformDTO);
    }


}
