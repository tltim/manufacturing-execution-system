package com.donger.mes.system.mes.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.donger.mes.system.mes.entity.WorkReportLog;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface WorkReportLogMapper extends BaseMapper<WorkReportLog> {
}
