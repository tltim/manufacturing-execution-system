package com.donger.mes.system.mes.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.donger.mes.system.mes.dto.WorkCenterMatPlatformDTO;
import com.donger.mes.system.mes.entity.WorkCenterMat;

import java.util.List;

public interface WorkCenterMatService {

    IPage<WorkCenterMat> page(IPage<WorkCenterMat> page, Wrapper<WorkCenterMat> queryWrapper);

    List<WorkCenterMatPlatformDTO> listGroupByWorkCenter(String planCode);


    Boolean save(WorkCenterMat workCenterMat);

    WorkCenterMat queryByBarcodeAndSupplier(String workCenterCode,String packageCode, String supplier,String planCode);

    List<WorkCenterMat> list(Wrapper<WorkCenterMat> queryWrapper);

    void updateBatchById(List<WorkCenterMat> updateMatList);

    void deleteBatchById(List<WorkCenterMat> deleteMatList);

    void returnMaterial(String workCenterCode,String planCode);

}
