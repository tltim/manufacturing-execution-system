package com.donger.mes.system.mes.dto;


import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 报工数据
 */
@Data
public class WorkDTO {
    @NotBlank(message = "工作中心编码不能为空")
    private String workCenterPlanCode;
    @NotBlank(message = "报工编码不能为空")
    private String packageCode;
    private Long num;
    private String isolate;
    // 返工返修数量
    private Long unqualifiedNum = 0L;

    @NotBlank(message = "报工类型不能为空")
    private String type;
}
