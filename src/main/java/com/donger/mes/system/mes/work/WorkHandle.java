package com.donger.mes.system.mes.work;


import com.donger.mes.system.base.entity.MesBaseBom;
import com.donger.mes.system.base.entity.MesBaseMaterial;
import com.donger.mes.system.base.entity.MesBaseWorkCenter;
import com.donger.mes.system.mes.dto.WorkDTO;
import com.donger.mes.system.mes.entity.WorkCenterPlan;

import java.util.List;

/**
 * 报工模式
 *
 *
 *
 *  根据不同的报工模式选择对应的装箱逻辑
 *  非一物一码整箱报工  一物一码不装箱报工   一物一码装箱报工
 *  根据工作中心的种类不同选择不同的物料倒冲模式
 *  倒冲模式为 本工作中心投料物料 ，  上级工作中心产出物料  ，  混合模式   三种模式   所有数据都在一起
 *  产出模式  产出产品供下级使用 ， 进入仓库   两种模式   两种模式如两个表
 *

 *  入口
 *
 *  执行逻辑
 *
 *
 *
 *  倒冲逻辑
 *
 *
 *  产成品逻辑
 *
 */
public interface WorkHandle {




    WorkCenterPlan execute(WorkCenterPlan workCenterPlan,WorkDTO workDTO);


    /**
     * 预处理
     * @param workCenterPlan
     * @param workDTO
     */
    void checkData(WorkCenterPlan workCenterPlan,WorkDTO workDTO);





    /**
     *
     * @param materialList bom 数据
     * @param produceNum 生产数量
     */
    void backflush(WorkCenterPlan workCenterPlan,List<MesBaseBom> materialList,Long produceNum);


    /**
     * 产出模式
     * @param type 产出类型
     * @param workCenterPlan 工作中心数据
     */
    void produce(String type,WorkCenterPlan workCenterPlan,WorkDTO workDTO,MesBaseMaterial mesBaseMaterial);



}
