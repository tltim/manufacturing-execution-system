package com.donger.mes.system.mes.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
public class PlanDayFormDTO {

    private Long id;

    @NotBlank(message = "订单号不能能为空")
    private String orderId;

    @NotNull(message = "计划时间不能为空")
    private LocalDateTime planStartDate;

    @NotEmpty(message = "产品不能为空")
    private String productCode;

    @NotNull(message = "生产数量不能为空")
    private Long productCount;

    @NotEmpty(message = "产线不能为空")
    private String linesCode;
}
