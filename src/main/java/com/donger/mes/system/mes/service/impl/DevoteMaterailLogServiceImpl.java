package com.donger.mes.system.mes.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.donger.mes.system.mes.entity.DevoteMaterailLog;
import com.donger.mes.system.mes.mapper.DevoteMaterailLogMapper;
import com.donger.mes.system.mes.service.DevoteMaterailLogService;
import org.springframework.stereotype.Service;

import javax.validation.Valid;

@Service
public class DevoteMaterailLogServiceImpl extends ServiceImpl<DevoteMaterailLogMapper, DevoteMaterailLog> implements DevoteMaterailLogService {

    /**
     * 新增投料记录
     * @param devoteMaterailLog
     */
    @Override
    public void saveDevoteLog(@Valid DevoteMaterailLog devoteMaterailLog) {
        // 校验数据



        // 存储数据


        this.save(devoteMaterailLog);

    }
}
