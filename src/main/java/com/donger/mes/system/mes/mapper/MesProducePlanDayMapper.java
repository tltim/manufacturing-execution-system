package com.donger.mes.system.mes.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.donger.mes.system.mes.entity.MesProducePlanDay;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author pmc
 */
@Mapper
public interface MesProducePlanDayMapper extends BaseMapper<MesProducePlanDay> {
}
