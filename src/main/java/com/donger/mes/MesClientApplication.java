package com.donger.mes;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author pmc
 */
@SpringBootApplication(exclude = {DruidDataSourceAutoConfigure.class})
@EnableFeignClients(basePackages = "com.donger.**")
@MapperScan(basePackages = {"com.gitee.sunchenbin.mybatis.actable.dao.*","com.donger.mes.**.mapper"})
@ComponentScan(basePackages = {"com.gitee.sunchenbin.mybatis.actable.manager.*","com.donger.mes.**"})
public class MesClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(MesClientApplication.class, args);
    }

}
